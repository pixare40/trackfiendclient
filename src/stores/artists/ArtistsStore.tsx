import {StoreBase, AutoSubscribeStore, autoSubscribe, key} from 'resub'
import ArtistClient from '../../request_clients/ArtistClient'
import TopArtistInfoModel = require('../../models/artists/TopArtistInfoModel')
import ArtistInfoModel = require('../../models/artists/ArtistInfoModel')

@AutoSubscribeStore
class ArtistStore extends StoreBase{
    private _topFollowed: Array<TopArtistInfoModel> = []
    private _loadingTopFollowed: boolean
    private _artistInfoDictionary: {[artist_id: string]: ArtistInfoModel} = {}
    private _currentlyViewedArtist: number

    @autoSubscribe
    getTopFollowed(): Array<TopArtistInfoModel>{
        return this._topFollowed;
    }

    @autoSubscribe
    getIsLoadingTopFollowed(): boolean{
        return this._loadingTopFollowed;
    }

    @autoSubscribe
    getArtistInfo(@key artist_id: number): ArtistInfoModel{
        return this._artistInfoDictionary[artist_id];
    }

    setCurrentViewedArtist(artist: number){
        this._currentlyViewedArtist = artist
    }

    @autoSubscribe
    getCurrentArtist(){
        return this._currentlyViewedArtist;
    }

    fetchTopFollowed(): void{
        this._loadingTopFollowed = true;
        this.trigger()

        ArtistClient.getTopFollowed().then(top_followed=>{
            this._topFollowed = top_followed.data;
            this._loadingTopFollowed = false;
            this.trigger()
        }, ()=>{
            this._loadingTopFollowed = false;
            this.trigger()
        })
    }

    fetchArtistInfo(artist_id: number): void{
        ArtistClient.getArtistInfo(artist_id).then(artist_info=>{
            this._artistInfoDictionary[artist_info.data.id] = artist_info.data
            this.trigger(artist_id)
        })
    }
}

export default new ArtistStore();