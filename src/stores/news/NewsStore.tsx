import { StoreBase, AutoSubscribeStore, autoSubscribe, key } from 'resub'
import StreamClient from '../../request_clients/StreamClient'
import NewsItem = require('../../request_clients/models/NewsItem')
import LoginStore from '../login/LoginStore'

@AutoSubscribeStore
class NewsStore extends StoreBase{
    private _newsItems: Array<NewsItem> = []
    private _isLoading: boolean = true;
    private _artistStream: {[artisId: number]: Array<NewsItem>} = {}

    @autoSubscribe
    getNewsItems(): Array<NewsItem>{
        return this._newsItems;
    }

    @autoSubscribe
    getArtistNewsItems(@key artistId: number): Array<NewsItem>{
        return this._artistStream[artistId] || [];
    }

    @autoSubscribe
    isLoading(): boolean{
        return this._isLoading;
    }

    fetchNewsItems(onError: ()=> void): void{
        this._isLoading = true;
        //this.trigger();

        let authToken = LoginStore.getAuthenticationToken()
         StreamClient.getStream(authToken).then(news_stream=>{
            this._newsItems = news_stream.data;
            this._isLoading = false;
            this.trigger()
        }, ()=>{
            onError();
        }).fail(()=>{
            onError();
        })
    }

    fetchArtistNews(artist_id: number): void{
        StreamClient.getArtistStream(artist_id).then(news_stream=>{
            this._artistStream[artist_id] = news_stream.data
            this.trigger(artist_id)
        })
    }
}

export default new NewsStore();