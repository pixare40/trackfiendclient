import { StoreBase, AutoSubscribeStore, autoSubscribe, key } from 'resub'
import TabButtonModel = require('../../models/TabButtonModel')

@AutoSubscribeStore
class MenuStore extends StoreBase{
    _menuDictionary: {[menuKey: string]: TabButtonModel[]} = {}

    @autoSubscribe
    getSelectedMenu(@key menuKey: string): TabButtonModel[]{
        return this._menuDictionary[menuKey]
    }

    setMenu(menuKey: string, buttons: TabButtonModel[]): void{
        this._menuDictionary[menuKey] = buttons
    }

    setAllToFalse(menuKey: string): void{
        let newMenuDictionary: TabButtonModel[] = []
        this._menuDictionary[menuKey].forEach(item=>{
            item.isSelected = false
            newMenuDictionary.push(item)
        })
        
        this._menuDictionary[menuKey] = newMenuDictionary;

        this.trigger(menuKey)
    }

    setSelectedMenu(menuKey: string, menuItem: string): void{
        if(!this._menuDictionary[menuKey]){
            return;
        }

        let newMenuDictionary: TabButtonModel[] = []
        this._menuDictionary[menuKey].forEach(item=>{
            if(item.title == menuItem){
                item.isSelected = true
                newMenuDictionary.push(item);
            }
            else{
                item.isSelected = false
                newMenuDictionary.push(item)
            }
        })
        this._menuDictionary[menuKey] = newMenuDictionary;

        this.trigger(menuKey)
    }
}

export default new MenuStore();