import { StoreBase, AutoSubscribeStore, autoSubscribe } from 'resub'
import ProfileClient from '../../request_clients/ProfileClient'
import UserInfoModel = require('../../models/user/UserInfoModel')
import LoginStore from '../login/LoginStore'
import ArtistInfoModel = require('../../models/artists/ArtistInfoModel')


@AutoSubscribeStore
class UserProfileStore extends StoreBase{
    private _userProfileInfo: UserInfoModel
    private _isFetchingProfileInfo: boolean = true

    @autoSubscribe
    getUserInfo(): UserInfoModel{
        return this._userProfileInfo
    }

    @autoSubscribe
    getIsFetchingUserInfo(): boolean{
        return this._isFetchingProfileInfo;
    }

    retrieveFollowingArtists(): ArtistInfoModel[]{
        return this._userProfileInfo.followed_artists;
    }

    fetchUserInfo() : void{
        this._isFetchingProfileInfo = true;
        this.trigger();

        let authToken = LoginStore.getAuthenticationToken()
        if(authToken == ''){
            this._isFetchingProfileInfo = false;
            return
        }
        ProfileClient.getUserProfileInfo(authToken).then(response => {
            this._userProfileInfo = response.data
            this._isFetchingProfileInfo = false
            this.trigger();
        })
    }

    isFollowingArtist(artistId: number): boolean{
        let isFollowing: boolean = false
        this._userProfileInfo.followed_artists.map(artist=>{
            if(artist.id == artistId){
                isFollowing = true
            }
        })

        return isFollowing;
    }
}

export default new UserProfileStore(); 