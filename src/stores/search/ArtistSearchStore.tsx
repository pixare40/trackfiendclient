import { StoreBase, AutoSubscribeStore, autoSubscribe } from 'resub'
import ArtistSearchClient from '../../request_clients/ArtistSearchClient'
import ArtistInfoModel = require('../../models/artists/ArtistInfoModel')

@AutoSubscribeStore
class ArtistSearchStore extends StoreBase{
    private _artistSearchResults: ArtistInfoModel[] = []

    @autoSubscribe
    getSearchResults(){
        return this._artistSearchResults;
    }

    fetchArtistSearchResults(searchTerm: string):void{
        if(searchTerm == ''){
            return;
        }
        
        ArtistSearchClient.search(searchTerm).then(results=>{
            this._artistSearchResults = results.data;
            this.trigger();
        })
    }
}

export default new ArtistSearchStore();