import { StoreBase, AutoSubscribeStore, autoSubscribe } from 'resub'

@AutoSubscribeStore
class HeaderStore extends StoreBase{
    private _headerTitle: string = "News"

    @autoSubscribe
    getHeaderTitle(): string{
        return this._headerTitle;
    }

    setHeaderTitle(headerTitle: string){
        this._headerTitle = headerTitle;
        this.trigger()
    }
}

export default new HeaderStore();