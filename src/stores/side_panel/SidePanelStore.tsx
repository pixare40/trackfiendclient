import { StoreBase, AutoSubscribeStore, autoSubscribe } from 'resub'
import RX = require('reactxp')

@AutoSubscribeStore
class SidePanelStore extends StoreBase{
    private _homePanelTranslateValue:RX.Animated.Value = new RX.Animated.Value(0);
    private _sidePanelTranslateValue:RX.Animated.Value = new RX.Animated.Value(257);

    @autoSubscribe
    getHomePanelTranslateValue(): RX.Animated.Value{
        return this._homePanelTranslateValue;
    }

    @autoSubscribe
    getSidePanelTranslateValue(): RX.Animated.Value{
        return this._sidePanelTranslateValue;
    }

    showSideMenu(): void{
        this._homePanelTranslateValue = new RX.Animated.Value(-257);
        this._sidePanelTranslateValue = new RX.Animated.Value(0);
        this.trigger();
    }

    hideSideMenu(): void{
        this._homePanelTranslateValue = new RX.Animated.Value(0);
        this._sidePanelTranslateValue = new RX.Animated.Value(257);
        this.trigger();
    }
}

export default new SidePanelStore();