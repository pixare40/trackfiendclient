import { StoreBase, AutoSubscribeStore, autoSubscribe } from 'resub'

@AutoSubscribeStore
class ViewExternalStore extends StoreBase{
    private _urlToRender: string
    private _reconfigure: boolean
    private _isLoadingExternalUrl: boolean

    setUrlToRender(url: string){
        this._urlToRender = url;
        this._isLoadingExternalUrl = true;
        this.trigger();
    }

    externalContentRendered(){
        this._isLoadingExternalUrl = false;
        this.trigger()
    }

    @autoSubscribe
    getUrlToRender(): string{
        return this._urlToRender;
    }

    @autoSubscribe
    getIsLoadingExternalUrl(): boolean{
        return this._isLoadingExternalUrl;
    }
}

export default new ViewExternalStore();