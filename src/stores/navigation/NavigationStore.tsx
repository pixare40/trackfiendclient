import { StoreBase, AutoSubscribeStore, autoSubscribe } from 'resub'
import NavigationRouteId = require('../../enums/NavigationRouteId')
import RX = require('reactxp')
import ViewExternalComponent = require('../../components/view_external/ViewExternalComponent')
import HomeComponent = require('../../components/home/HomeComponent')

interface NavigationContext{

}

@AutoSubscribeStore
class NavigationStore extends StoreBase{
    private _navigatorInstance: RX.Navigator
    private _currentRouteComponent: JSX.Element
    private _navigationContext: {[panelId: string]: {}} = {}
    private _sceneContext: any = {}
    private _panelToRoute: string = 'News'

    setNavigationInstance(navigationInstance: RX.Navigator){
        this._navigatorInstance = navigationInstance;
    }

    goHome(){
        this._navigatorInstance.immediatelyResetRouteStack([{
            routeId: NavigationRouteId.HomePanel,
            sceneConfigType: RX.Types.NavigatorSceneConfigType.Fade
        }])
    }

    goToRoute(navigationRoute: NavigationRouteId, 
                sceneConfigType: RX.Types.NavigatorSceneConfigType,
                routeProps: any ){
        this._sceneContext[navigationRoute] = routeProps;
        this._navigatorInstance.push({
            routeId: navigationRoute,
            sceneConfigType: sceneConfigType
        })
    }

    goBack(){
        this._navigatorInstance.pop();
    }

    @autoSubscribe
    getRoutePanel(): string{
        return this._panelToRoute;
    }

    setRoutePanel(panel: string, panelContext: any){
        this._panelToRoute = panel;
        this._navigationContext[panel] = panelContext
        this.trigger()
    }

    getPanelContext(panelId: string): {}{
        return this._navigationContext[panelId]
    }

    getSceneContext(sceneId: NavigationRouteId): {}{
        return this._sceneContext[sceneId];
    }
}

export default new NavigationStore();