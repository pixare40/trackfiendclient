import { StoreBase, AutoSubscribeStore, autoSubscribe } from 'resub'
import LoginClient from '../../request_clients/LoginClient'

@AutoSubscribeStore
class LoginStore extends StoreBase{
    private _isLoggedIn: boolean = false;
    private _authenticationToken: string;

    @autoSubscribe
    isLoggedIn(): boolean{
        return this._isLoggedIn;
    }

    @autoSubscribe
    getAuthenticationToken():string{
        return this._authenticationToken;
    }

    getLoggedInStatus():boolean{
        return this._isLoggedIn;
    }

    login(email: string, password: string): void{
        LoginClient.login(email, password).then((authenticationToken) =>{
            this._authenticationToken = authenticationToken.jwt
            this._isLoggedIn = true
            this.trigger()
        });
    }

    register(email: string, password: string, firstname: string, lastname: string): any{
        return LoginClient.register(email,password,firstname,lastname)
    }
}

export default new LoginStore();