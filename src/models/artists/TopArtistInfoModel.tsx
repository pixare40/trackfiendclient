interface TopArtistInfoModel{
    name: string,
    followers: number,
    artist_image_url: string,
    artist_id: number
}

export = TopArtistInfoModel;