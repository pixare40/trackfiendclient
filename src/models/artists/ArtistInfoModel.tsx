import NewsItem = require ('../../request_clients/models/NewsItem')

interface ArtistInfoModel{
    id: number,
    name: string,
    realname: string,
    cname: string,
    followers: number,
    artist_image_url: string,
    news: NewsItem[]
}

export = ArtistInfoModel;