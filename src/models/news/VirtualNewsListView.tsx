import { VirtualListViewItemInfo } from 'reactxp-virtuallistview'
import NewsItem = require ('../../request_clients/models/NewsItem')

interface VirtualNewsListView extends VirtualListViewItemInfo{
    newsItem: NewsItem
}


export = VirtualNewsListView;