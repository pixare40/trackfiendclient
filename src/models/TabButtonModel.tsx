import RX = require('reactxp')

interface TabButtonModel{
    title: string,
    iconPath?: string,
    viewBox?: string,
    isSelected: boolean,
    height?: number,
    width?: number,
    showIcon: boolean,
    textContent?: string,
    menuKey: string,
    textStyles?: RX.Types.TextStyleRuleSet
}

export = TabButtonModel;