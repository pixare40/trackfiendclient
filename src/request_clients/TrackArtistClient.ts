import { GenericRestClient } from 'simplerestclients';
import SyncTasks = require('synctasks')

interface TrackArtistResponse{
}

const _loginApi = 'http://192.168.1.65:4000/api/v1/track-artist'

class TrackArtistClient extends GenericRestClient{

    trackArtist(artist_id: number, authToken: string): SyncTasks.Promise<TrackArtistResponse>{
        return this.performApiPost<TrackArtistResponse>('', {artist_id: artist_id}, {headers: {"Authorization":"Bearer "+ authToken}}).then(
            response =>{
                return response
            }
        )
    }
}

export default new TrackArtistClient(_loginApi);