import { GenericRestClient } from 'simplerestclients';
import SyncTasks = require('synctasks')
import ArtistInfoModel = require('../models/artists/ArtistInfoModel')

interface ArtistSearchResponse{
    data: ArtistInfoModel[]
}

const _artistSearchApi = 'http://192.168.1.65:4000/api/v1/search-artist'

class ArtistSearchClient extends GenericRestClient{

    search(searchTerm: string): SyncTasks.Promise<ArtistSearchResponse>{
        return this._performApiCall<ArtistSearchResponse>('', 'POST',  {search_term: searchTerm}, undefined).then(response=>{
            return response.body
        })
    }
}

export default new ArtistSearchClient(_artistSearchApi);