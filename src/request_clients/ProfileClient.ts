import { GenericRestClient } from 'simplerestclients';
import SyncTasks = require('synctasks')
import UserInfoModel = require('../models/user/UserInfoModel')

interface UserProfileResponse{
    data: UserInfoModel
}

const _loginApi = 'http://192.168.1.65:4000/api/v1/user-info'

class ProfileClient extends GenericRestClient{

    getUserProfileInfo(authToken: string): SyncTasks.Promise<UserProfileResponse>{
        return this.performApiGet<UserProfileResponse>('', {headers: {"Authorization":"Bearer "+ authToken}}).then(
            response =>{
                return response
            }
        )
    }
}

export default new ProfileClient(_loginApi);