import { GenericRestClient } from 'simplerestclients';
import SyncTasks = require('synctasks')

interface LoginResponse{
    jwt:string
}

interface RegistrationResponse{
    data: any
}

const _authApi = 'http://192.168.1.65:4000/api/v1'

class LoginClient extends GenericRestClient{

    login(username: string, password: string): SyncTasks.Promise<LoginResponse>{
        return this._performApiCall<LoginResponse>( '/log-in', 'POST', {email:username, password: password }, undefined).then(response =>{
            return response.body
        })
    }

    register(email: string, password: string, firstname: string, lastname: string): SyncTasks.Promise<RegistrationResponse>{
        return this.performApiPost<RegistrationResponse>('/register', {email: email, 
                                                                    password: password,
                                                                firstname: firstname,
                                                            lastname: lastname}).then(response=>{
            return response;                                                        
        })
    }
}

export default new LoginClient(_authApi);