import { GenericRestClient } from 'simplerestclients';
import SyncTasks = require('synctasks')
import TopArtistInfoModel = require('../models/artists/TopArtistInfoModel')
import ArtistInfoModel = require('../models/artists/ArtistInfoModel')

interface TopArtistResponse{
    data: TopArtistInfoModel[]
}

interface ArtistInfoResponse{
    data: ArtistInfoModel
}

const _artistFollowersApi = 'http://192.168.1.65:4000/api/v1'
const _topFollowedEndPoint = '/top-followed-artists'
const _artistInfoEndpoint = '/artist-info'

class ArtistClient extends GenericRestClient{

    getTopFollowed(): SyncTasks.Promise<TopArtistResponse>{
        return this.performApiGet<TopArtistResponse>(_topFollowedEndPoint)
        .then(response=>{
            return response
        })
    }

    getArtistInfo(artistId: number): SyncTasks.Promise<ArtistInfoResponse>{
        return this.performApiGet<ArtistInfoResponse>(_artistInfoEndpoint + '/' + artistId)
        .then(response=>{
            return response
        })
    }
}

export default new ArtistClient(_artistFollowersApi);