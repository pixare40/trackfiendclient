import { GenericRestClient } from 'simplerestclients';
import RX = require('reactxp')
import SyncTasks = require('synctasks')
import NewsItem = require('./models/NewsItem')

interface StreamResponse{
    data: Array<NewsItem>
}

class StreamClient extends GenericRestClient{
    getStream(authToken: string): SyncTasks.Promise<StreamResponse>{
        return this.performApiGet<StreamResponse>('/stream', {headers: {"Authorization":"Bearer "+ authToken}}).then(response=>{
            return response 
        })
    }

    getArtistStream(artist_id: number): SyncTasks.Promise<StreamResponse>{
        return this.performApiGet<StreamResponse>('/artist-stream/'+ artist_id).then(response=>{
            return response 
        })
    }
}

export default new StreamClient('http://192.168.1.65:4000/api/v1')