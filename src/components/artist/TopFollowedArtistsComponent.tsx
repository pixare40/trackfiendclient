import { ComponentBase } from 'resub'
import RX = require('reactxp')
import ArtistStore from '../../stores/artists/ArtistsStore'
import TopArtistInfoModel = require('../../models/artists/TopArtistInfoModel')
import ArtistTileItemComponent = require('./ArtistTileItemComponent')
import TrackFiendStyles = require('../../TrackFiendStyles')
import NavigationStore from '../../stores/navigation/NavigationStore'
import NavigationRouteId = require('../../enums/NavigationRouteId')

interface TopFollowedArtistsState{
    followedArtists: TopArtistInfoModel[]
    isLoadingTopFollowed: boolean
}

const _styles = {
    tileContainerStyles: RX.Styles.createViewStyle({
        flexDirection: 'row',
        flexWrap: 'wrap' 
    }),

    tileItemStyles: RX.Styles.createViewStyle({
        width: RX.UserInterface.measureWindow().width/2 - 5,
        height: 150,
        justifyContent: 'center',
        borderColor: TrackFiendStyles.color.appBackgroundColour,
        borderWidth: 2,
        borderStyle: 'solid',
        marginBottom: 4
    })
}

class TopFollowedArtistsComponent extends ComponentBase<{}, TopFollowedArtistsState>{
    protected _buildState(props: {}, initialBuild: boolean): TopFollowedArtistsState{
        return {
            followedArtists: ArtistStore.getTopFollowed(),
            isLoadingTopFollowed: ArtistStore.getIsLoadingTopFollowed()
        }
    }

    componentDidMount(){
        ArtistStore.fetchTopFollowed()
    }

    render(): JSX.Element|null{
        return(
            <RX.ScrollView>
                <RX.View style={ _styles.tileContainerStyles }>
                    { this.renderTileItem() }
                </RX.View>
            </RX.ScrollView>
        )
    }

    renderTileItem(){
        return(
            this.state.followedArtists.map((artist, index)=>{
                return <ArtistTileItemComponent onArtistTapped={ this.onArtistTapped }
                                            artist={ artist }
                                            styles={ _styles.tileItemStyles }
                                            key={ artist.artist_id }/>
            })
        )
    }

    onArtistTapped = (artist_id: number) =>{
        ArtistStore.setCurrentViewedArtist(artist_id)
        NavigationStore.goToRoute(NavigationRouteId.Artist,
            RX.Types.NavigatorSceneConfigType.FloatFromRight , 
            {'artist_id': artist_id})
    }
}

export = TopFollowedArtistsComponent;