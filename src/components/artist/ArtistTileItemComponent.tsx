import { ComponentBase } from 'resub'
import RX = require('reactxp')
import TopArtistInfoModel = require('../../models/artists/TopArtistInfoModel')
import TrackFiendStyles = require ('../../TrackFiendStyles')

interface ArtistTileItemProps{
    onArtistTapped: (artist_id: number) => void
    artist: TopArtistInfoModel,
    styles: RX.Types.StyleRuleSet<RX.Types.ViewStyle>
    key: number
}

const _styles ={
    imageStyles: RX.Styles.createViewStyle({
        height: 150
    }),

    textStyles: RX.Styles.createTextStyle({
        fontSize: 11,
        position: 'absolute',
        bottom: 0,
        color: TrackFiendStyles.color.titleTextColor,
        padding: 5,
        shadowColor: TrackFiendStyles.color.defaultShadowColor,
        backgroundColor: TrackFiendStyles.color.titleTextBackgroundColour
    })
}

class ArtistTileItemComponent extends ComponentBase<ArtistTileItemProps, null>{

        //TODO [ Extremely Ugly ] Should make comparison before force updating component to improve perf
    shouldComponentUpdate(nextProps: ArtistTileItemProps, nextState: null){
        return true;
    }

    render(): JSX.Element | null{
        return(
            <RX.View style={ this.props.styles } onPress={ this.goToArtist }>
                {this.renderArtistImage()}
                <RX.Text style={ _styles.textStyles }>
                    { this.props.artist.name }
                </RX.Text>
            </RX.View>
        )
    }

    goToArtist = ()=>{
        this.props.onArtistTapped(this.props.artist.artist_id)
    }

    renderArtistImage(){
        if(this.props.artist.artist_image_url){
            return (
                <RX.Image source={ this.props.artist.artist_image_url } 
                            resizeMode={ 'cover' }
                            style={ _styles.imageStyles }/>
            )
        }
        else{
            return (
                <RX.Image source={ '' } 
                            resizeMode={ 'cover' }
                            style={ _styles.imageStyles }/>
            )
        }
    }
}

export = ArtistTileItemComponent;