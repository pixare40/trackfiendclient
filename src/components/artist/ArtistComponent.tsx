import { ComponentBase } from 'resub'
import RX = require('reactxp')
import TrackFiendStyles = require('../../TrackFiendStyles')
import TopArtistInfoModel = require('../../models/artists/TopArtistInfoModel')
import ArtistStore from '../../stores/artists/ArtistsStore'
import ArtistInfoModel = require('../../models/artists/ArtistInfoModel')
import ArtistNewsComponent = require('../news/ArtistNewsComponent')
import TrackFiendIcons = require('../../TrackFiendIcons')
import { default as RXImageSvg, SvgPath as RXSvgPath } from 'reactxp-imagesvg';
import NavigationStore from '../../stores/navigation/NavigationStore'

interface ArtistComponentProps{
    artist_id: number
}

interface ArtistComponentState{
    artistInfo: ArtistInfoModel
}

const _styles = {
    rootComponentStyles: RX.Styles.createViewStyle({
        flex: 1,
        backgroundColor: TrackFiendStyles.color.appBackgroundColour
    }),

    artistHeader: RX.Styles.createViewStyle({
        flexDirection: 'row',
        padding: 10
    }),

    artistImageStyle: RX.Styles.createImageStyle({
        borderRadius: 25,
        height: 50,
        width: 50
    }),

    artistHeaderTextContainer: RX.Styles.createViewStyle({
        paddingLeft: 30
    }),

    artistTrackersStyles: RX.Styles.createTextStyle({
        fontSize: 12
    }),

    backButtonTextStyles: RX.Styles.createTextStyle({
        color: TrackFiendStyles.color.titleTextColor,
        fontWeight: 'normal',
        fontSize: 11
    }),


}

class ArtistComponent extends ComponentBase<{}, ArtistComponentState>{

    private _props: ArtistComponentProps
    
    constructor(props: {}){
        super(props)
        this._props = props as ArtistComponentProps
    }

    protected _buildState(props: ArtistComponentProps, initialBuild: boolean): 
            ArtistComponentState
    {
        return {
            artistInfo: ArtistStore.getArtistInfo(this._props.artist_id)
        }
    }

    componentDidMount(){
        if(this.state.artistInfo){
            return;
        }

        ArtistStore.fetchArtistInfo(this._props.artist_id)
    }

    render(): JSX.Element | null{
        return(
            this.renderView()
        )
    }

    renderView(){
        if(this.state.artistInfo)
        {
            return(
                <RX.View style={ _styles.rootComponentStyles }>
                    <RX.View style={ [TrackFiendStyles.styles.header, 
                                    RX.StatusBar.isOverlay() && 
                                    TrackFiendStyles.styles.headerWithStatusBar, 
                                    TrackFiendStyles.styles.defaultBackButtonContainerHeaderStyles] }>
                        <RX.Button onPress={ this.goBack } 
                                    style={ TrackFiendStyles.styles.defaultBackButtonStyles }>
                            <RXImageSvg viewBox = { TrackFiendIcons.icons.chevron_left.viewBox } 
                                        height={20} width={20}>
                                <RXSvgPath d={ TrackFiendIcons.icons.chevron_left.path }
                                            fillColor= {'white'}/>
                            </RXImageSvg>
                            <RX.Text style={ _styles.backButtonTextStyles }>
                                Back
                            </RX.Text>
                        </RX.Button>
                    </RX.View>
                    <RX.View style={ _styles.artistHeader }>
                        { this.renderArtistImage() }
                        <RX.View style={ _styles.artistHeaderTextContainer }>
                            <RX.Text>
                                { this.state.artistInfo.name }
                            </RX.Text>
                            <RX.Text style={ _styles.artistTrackersStyles }>
                               {this.renderArtistTrackerText()}
                            </RX.Text>
                        </RX.View>
                    </RX.View>
                    
                    <ArtistNewsComponent />
                </RX.View>
            )
        }

        return (<RX.ActivityIndicator color={ TrackFiendStyles.color.mainAppColor } 
                                size={ 'medium' }/>)
    }

    renderArtistTrackerText = () => {
        if(this.state.artistInfo.followers == 1){
            return this.state.artistInfo.followers + " Tracker"
        }
        else{
            return this.state.artistInfo.followers + " Trackers"
        }
    }

    renderArtistImage = () =>{
        if(this.state.artistInfo.artist_image_url){
            return(
                <RX.Image source={ this.state.artistInfo.artist_image_url }
                            style={ _styles.artistImageStyle }
                            resizeMode={ 'cover' } />
            )
        }else{
            return(
                <RX.Image source={ TrackFiendIcons.images.personPlaceholder.url }
                            style={ _styles.artistImageStyle }
                            resizeMode={ 'cover' } />
            )
        }
    }

    goBack = () =>{
        NavigationStore.goBack();
    }
}

export = ArtistComponent;