import { ComponentBase } from 'resub'
import RX = require('reactxp')
import TopArtistInfoModel = require('../../models/artists/TopArtistInfoModel')
import ArtistTileItemComponent = require('./ArtistTileItemComponent')

interface ArtistTileListProps{
    onArtistTapped: (artist_id: number) => void
    artistList: Array<TopArtistInfoModel>
}

const _styles = {
    tileContainerStyles: RX.Styles.createViewStyle({
        flexDirection: 'row',
        flexWrap: 'wrap' 
    }),

    tileItemStyles: RX.Styles.createViewStyle({
        flex: 0.5,
        height: 100,
        justifyContent: 'center'
    })
}

class ArtistTileListComponent extends ComponentBase<ArtistTileListProps, null>{
    render(): JSX.Element | null{
        return(
            <RX.View>
                { this.renderTileItem() }
            </RX.View>
        )
    }

    renderTileItem = ()=>{
        this.props.artistList.forEach((artist, index)=>{
            return <ArtistTileItemComponent onArtistTapped={ this.props.onArtistTapped }
                                        artist={ artist }
                                        styles={ _styles.tileItemStyles }
                                        key={ artist.artist_id }/>
        })
    }
}

export = ArtistTileListComponent;