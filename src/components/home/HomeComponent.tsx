import { ComponentBase } from 'resub';
import RX = require('reactxp');
import TrackFiendStyles = require ('../../TrackFiendStyles');
import NewsComponent = require ('../news/NewsComponent');
import BottomBarComponent = require ('../bottomBar/BottomBarComponent');
import ExchangeComponent = require('../exchange/ExchangeComponent');
import LoginComponent = require('../login/LoginComponent');
import MenuStore from '../../stores/menu/MenuStore';
import SidePanelStore from '../../stores/side_panel/SidePanelStore';
import LoginSwitcherComponent = require('../switcher/LoginSwitcherComponent');
import ArtistSearchComponent = require('../search/ArtistSearchComponent');
import ProfileComponent = require('../profile/ProfileComponent');
import HeaderStore from '../../stores/home/HeaderStore';
import ArtistComponent = require('../artist/ArtistComponent');
import NavigationStore from '../../stores/navigation/NavigationStore';
import RegistrationComponent = require('../login/RegistrationComponent');

interface HomeComponentProps{
}

const _styles = {
    appName: RX.Styles.createViewStyle({
        alignSelf: 'center',
        alignItems: 'center',
    }),

    appNameText: RX.Styles.createTextStyle({
        textAlign: 'center',
        color: TrackFiendStyles.color.titleTextColor,
        fontFamily: RX.Platform.getType() == "web"? "Righteous": "Righteous-Regular",
        fontSize: 25
    }),

    bottomBarContainer: RX.Styles.createViewStyle({
        alignSelf: 'stretch',
        bottom: 0,
        flex: 1,
        position: 'absolute',
    }),

    rootStyle: RX.Styles.createViewStyle({
        flex: 1,
        backgroundColor: TrackFiendStyles.color.appBackgroundColour,
        flexDirection: 'row',
        shadowColor: 'black',
        shadowOffset: {width:2, height: 0}
    }),

    loginButton: RX.Styles.createButtonStyle({
        marginLeft: 10,
        position: 'absolute',
        right: 0,
        paddingRight: 15,
        height: 44
    }),

    loginText: RX.Styles.createTextStyle({
        color: TrackFiendStyles.color.white
    }),

    scrollView: RX.Styles.createScrollViewStyle({
        marginBottom: 44, 
        alignItems: 'stretch',
        flex: 1
    })
}

interface HomeState{
    selectedPanel: string,
    headerTitle: string,
    homeComponentXValue: RX.Animated.Value,
    sideMenuComponentXValue: RX.Animated.Value,
    homeComponentPointerEventsDisabled: boolean,
    sideMenuVisible: boolean
}

class HomeComponent extends ComponentBase<HomeComponentProps, HomeState>{
    protected _buildState(props: HomeComponentProps, initialBuild: boolean): HomeState{
        return {
            selectedPanel: NavigationStore.getRoutePanel(),
            headerTitle: HeaderStore.getHeaderTitle(),
            homeComponentXValue: SidePanelStore.getHomePanelTranslateValue(),
            sideMenuComponentXValue: SidePanelStore.getSidePanelTranslateValue(),
            homeComponentPointerEventsDisabled: false,
            sideMenuVisible: false
        }
    }

    shouldComponentUpdate(nextProps: HomeComponentProps, nextState: HomeState){
        return true;
    }

    render(): JSX.Element | null{
        let homeViewAnimatedStyle = RX.Styles.createAnimatedViewStyle({
            transform: [{
                translateX: this.state.homeComponentXValue
            }],
            width: new RX.Animated.Value(RX.UserInterface.measureWindow().width)
        });
        let sideViewAnimatedStyle = RX.Styles.createAnimatedViewStyle({
            transform: [{
                translateX: this.state.sideMenuComponentXValue
            }],
            width: new RX.Animated.Value(257)
        })
        return (
            <RX.View style={[_styles.rootStyle, 
                {width: RX.UserInterface.measureWindow().width + 257}]}
                onPress={ this.onHomePanelTapped }
                disableTouchOpacityAnimation={ true }>
                <RX.Animated.View style={ homeViewAnimatedStyle }
                    disableTouchOpacityAnimation={ true }>
                    <RX.View style={ [TrackFiendStyles.styles.header, 
                                    RX.StatusBar.isOverlay() && 
                                    TrackFiendStyles.styles.headerWithStatusBar] }>
                        <RX.Text style={ _styles.appNameText }>
                            {this.state.headerTitle}
                        </RX.Text>
                        <LoginSwitcherComponent onLoginTapped= { this.onLoginPressed }
                            onProfileTapped = { this.onProfileTapped }/>
                    </RX.View>

                    <RX.View style= { _styles.scrollView } className={ "main-container" } 
                        blockPointerEvents={ this.state.homeComponentPointerEventsDisabled }
                        ignorePointerEvents={ this.state.homeComponentPointerEventsDisabled }>
                        { this._renderPanel(this.state.selectedPanel) }
                    </RX.View>

                    <RX.View onLayout={ this.onLayout } 
                            style= { [_styles.bottomBarContainer,{width: RX.UserInterface.measureWindow().width}] }>
                        <BottomBarComponent onBottomButtonPressed= { this.onTabPressed }/>
                    </RX.View>
                </RX.Animated.View>
                <RX.Animated.View style={ sideViewAnimatedStyle }
                    disableTouchOpacityAnimation={ true }>
                    <RX.Text>
                        { 'Side Menu' }
                    </RX.Text>
                </RX.Animated.View>
            </RX.View>
            
        );
    }

    onLayout =()=>{
        this.forceUpdate();
    }

    onHomePanelTapped = () =>{
        if(this.state.sideMenuVisible){
            SidePanelStore.hideSideMenu();
            this.setState(Object.assign({sideMenuVisible : false,
                homeComponentPointerEventsDisabled: false}))
        }
    }

    onTabPressed = (tab: string) => {
        if(tab=='Menu'){
            if(!this.state.sideMenuVisible){
                SidePanelStore.showSideMenu();
                this.setState( Object.assign({sideMenuVisible : true,
                    homeComponentPointerEventsDisabled: true}) );
            }
            else{
                SidePanelStore.hideSideMenu();
                this.setState( Object.assign({sideMenuVisible : false,
                    homeComponentPointerEventsDisabled: false}) );
            }
            return
        }
        this.setState( Object.assign({selectedPanel : tab, headerTitle: tab}) );
    }

    onLoginPressed = () => {
        MenuStore.setAllToFalse('bottombarmenu')
        this.setState(Object.assign({selectedPanel: 'Login', headerTitle: 'Login'}));
    }

    goToStream = () =>{
        this.setState(Object.assign({selectedPanel: 'News', headerTitle: 'News'}));
    }

    onProfileTapped = () => {
        this.setState(Object.assign({selectedPanel: 'Profile', headerTitle: 'Profile'}));
    }

    navigate = (panelName: string) =>{
        this.setState(Object.assign({selectedPanel: panelName, headerTitle: panelName}));
    }

    _renderPanel(selectedPanel: string){
        let panelContext = NavigationStore.getPanelContext(selectedPanel)        
        if(selectedPanel== 'News'){
            return <NewsComponent />
        }
        else if(selectedPanel == 'Exchange'){
            return <ExchangeComponent/>
        }
        else if(selectedPanel == 'Login'){
            return <LoginComponent />
        }
        else if(selectedPanel == 'Search'){
            return <ArtistSearchComponent />
        }
        else if(selectedPanel == 'Profile'){
            return <ProfileComponent />
        }
        else if(selectedPanel == 'ArtistInfo'){
            return <ArtistComponent {...panelContext}/>
        }
        else if(selectedPanel == 'Registration'){
            return <RegistrationComponent />
        }
        else{
            return <NewsComponent />
        }
    }
}

export = HomeComponent;