import { ComponentBase } from 'resub'
import RX = require('reactxp')
import NewsItemComponent = require('./NewsItemComponent')
import TrackFiendStyles = require ('../../TrackFiendStyles')

const _jumboStyles ={
    mainComponentStyles: RX.Styles.createViewStyle({
        height: 280,
        marginTop: 5,
        paddingLeft: 5,
        paddingRight: 5,
        marginBottom: 5,
        flex: 1,
        backgroundColor: TrackFiendStyles.color.appBackgroundColour,
    }),

    mainTitleStyle: RX.Styles.createTextStyle({
        fontSize: 12,
        fontWeight: 'bold',
        position: 'absolute',
        bottom: 0,
        color: TrackFiendStyles.color.white,
        padding: 15,
        shadowColor: TrackFiendStyles.color.defaultShadowColor,
        backgroundColor: TrackFiendStyles.color.titleTextBackgroundColour
    }),

    jumboImageStyle: RX.Styles.createImageStyle({
        height: 280
    }),

    jumboComponentStyles: RX.Styles.createViewStyle({
        flexDirection: 'row',
        alignItems: 'stretch'
    })
}

class JumboComponent extends NewsItemComponent{
    render(): JSX.Element | null{
        return(
            <RX.View style={ _jumboStyles.mainComponentStyles } 
                        onPress={ this.onStoryTap }>

                <RX.Image source={ this.props.newsItem.news_item_image } 
                            resizeMode={ 'cover' }
                            style={ _jumboStyles.jumboImageStyle }>
                </RX.Image>

                <RX.Text style={ _jumboStyles.mainTitleStyle }>
                    {this.props.newsItem.title}
                </RX.Text>

            </RX.View>
        )
    }
}

export = JumboComponent;