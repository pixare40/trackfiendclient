import { ComponentBase } from 'resub'
import RX = require('reactxp')
import NewsItem = require('../../request_clients/models/NewsItem')
import TrackFiendStyles = require ('../../TrackFiendStyles')
import ViewExternalStore from '../../stores/view_external/ViewExternalStore'
import NavigationStore from '../../stores/navigation/NavigationStore'
import NavigationRouteId = require('../../enums/NavigationRouteId')

const _styles = {
    mainComponentStyles: RX.Styles.createViewStyle({
        position: 'relative',
        height: 100,
        flexDirection: 'row',
        alignItems: 'stretch',
        marginTop: 0,
        paddingLeft: 5,
        paddingRight: 5,
        marginBottom: 5,
        backgroundColor: TrackFiendStyles.color.appBackgroundColour,
        borderColor: TrackFiendStyles.color.cardBorderColor,
        borderWidth: 1,
        borderStyle: 'solid',
    }),

    storyContainerStyles: RX.Styles.createViewStyle({
        height: 100,
        flexDirection: 'row',
        flexWrap: 'nowrap',
        alignItems: 'stretch',
        flex: 0.3
    }),

    newsImageStyles: RX.Styles.createImageStyle({
        height: 100,
        width: 130
    }),

    titleTextStyles: RX.Styles.createTextStyle({
        textAlign: 'left',
        margin: 5,
        flex: 0.7
    })
}

interface NewsItemProps{
    key: string
    newsItem: NewsItem
}

class NewsItemComponent extends ComponentBase<NewsItemProps, null>{
    protected _buildState(props: NewsItemProps, initialBuild: boolean): null{
        return null
    }

    render(): JSX.Element | null{
        return(
            <RX.View onPress={ this.onStoryTap } style= { _styles.mainComponentStyles }>
                <RX.View style={ _styles.storyContainerStyles }>
                    <RX.Image source={ this.props.newsItem.news_item_image } 
                                style={ _styles.newsImageStyles } 
                                resizeMode={ 'cover' }>
                    </RX.Image>

                    <RX.Text style={ [TrackFiendStyles.styles.defaultTextStyles, 
                                        _styles.titleTextStyles] } 
                                numberOfLines={ 2 }>
                        {this.props.newsItem.title}
                    </RX.Text>
                </RX.View>
            </RX.View>
        )
    }

    onStoryTap = () =>{
        ViewExternalStore.setUrlToRender(this.props.newsItem.url_to_story);
        NavigationStore.goToRoute(NavigationRouteId.ViewExternal,
                                    RX.Types.NavigatorSceneConfigType.FloatFromRight,
                                null);
    }
}

export = NewsItemComponent;