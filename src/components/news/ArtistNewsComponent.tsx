import { ComponentBase } from 'resub';
import RX = require('reactxp');
import NewsComponent = require('./NewsComponent');
import NewsState = require('../../state/NewsState')
import ArtistStore from '../../stores/artists/ArtistsStore';
import NewsStore from '../../stores/news/NewsStore';

interface ArtistNewsComponentProps{
    artistId: number
}

class ArtistNewsComponent extends NewsComponent{
    protected _buildState(props:{}, initialBuild: boolean): NewsState{
        let artistId = ArtistStore.getCurrentArtist()
        return{
            news: ArtistStore.getArtistInfo(artistId).news,
            isLoadingNews: false,
            virtualListNews: ArtistStore.getArtistInfo(artistId).news.map((item, index)=>{
                return {
                    index: index,
                    key: item.url_to_story + item.title,
                    height: this.getItemHeight(item, index),
                    template: 'news item',
                    newsItem: item
                }
            })
        }
    }

    componentDidMount(){
        let artistId = ArtistStore.getCurrentArtist()
        ArtistStore.fetchArtistInfo(artistId)
    }
}

export = ArtistNewsComponent;