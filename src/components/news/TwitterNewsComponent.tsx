import { ComponentBase } from 'resub'
import NewsItemComponent = require('./NewsItemComponent')
import TweetStatusItem  = require('../../request_clients/models/TweetStatusItem')
import NewsItem = require ('../../request_clients/models/NewsItem')
import RX = require('reactxp')
import { default as RXVideo } from 'reactxp-video'
import TrackFiendStyles = require('../../TrackFiendStyles')
import TrackFiendIcons = require('../../TrackFiendIcons')
import TwitterUserKeysValueObject = require('../../value_objects/TwitterUserKeysValueObject')
import { default as RXImageSvg, SvgPath as RXSvgPath } from 'reactxp-imagesvg';

const _styles={
    tweetImageStyle: RX.Styles.createImageStyle({
        height: 200
    }),

    videoStyles: RX.Styles.createViewStyle({
        height: 200,
        maxHeight: 200
    }),

    tweetTextStyles: RX.Styles.createTextStyle({
        fontSize: 11,
        flex: 1
    }),

    tweetContainerStyles: RX.Styles.createViewStyle({
        marginTop:0,
        marginBottom: 5,
        paddingLeft: 5,
        paddingRight: 5,
        backgroundColor: 'white',
        borderColor: TrackFiendStyles.color.cardBorderColor,
        borderWidth: 1,
        borderStyle: 'solid',
    }),

    tweetDescriptionStyles: RX.Styles.createTextStyle({
        fontSize: 11
    }),

    tweetMediaContainer: RX.Styles.createViewStyle({
        height: 283
    }),

    tweetTextContainer: RX.Styles.createTextStyle({
        height: 98,
        padding: 5,
        flex: 1,
        flexDirection: 'row'
    }),

    tweetDescriptionContainer: RX.Styles.createViewStyle({
        flexDirection: 'row',
        height: 85,
        flex: 1,
        padding: 5
    }),

    userTwitterName: RX.Styles.createTextStyle({
        fontSize: 12,
        fontWeight: 'bold',
        flex: 1,
        width: RX.UserInterface.measureWindow().width - 82,
    }),

    userTwitterUserHandle: RX.Styles.createTextStyle({
        fontSize: 10
    }),

    userNameContainerStyles: RX.Styles.createViewStyle({
        flexDirection: 'row'
    }),

    twitterIconStyles: RX.Styles.createViewStyle({
        alignSelf: 'flex-end',
        justifyContent: 'flex-end',
        flex: 1
    }),

    profilePictureContainer: RX.Styles.createViewStyle({
        height: 85,
        width: 40
    }),

    profilePictureStyles: RX.Styles.createImageStyle({
        height: 30,
        width: 30,
        borderRadius: 4
    })
}

class TwitterTextNewsComponent extends NewsItemComponent{
    private _videoComponent: RXVideo;
    private _isPlaying: boolean = false;

    render(): JSX.Element | null{
        return(
            <RX.View style={ _styles.tweetContainerStyles }>
                { this.renderTwitterItem() }
            </RX.View>
        )
    }

    playVideo = () =>{
        if(this._isPlaying){
            this._videoComponent.pause()
            this._isPlaying = false;
        }
        else{
            this._videoComponent.play();
            this._isPlaying = true;
        }
    }

    renderTwitterItem = () =>{
        let twitter_status_item = this.props.newsItem as TweetStatusItem
        if(twitter_status_item.has_media!= null && twitter_status_item.has_media 
            && twitter_status_item.media_type == 'photo'){
                return(
                    <RX.View style={ _styles.tweetMediaContainer }>
                        <RX.Image source={ twitter_status_item.media_url_https }
                                    resizeMode={ 'auto' } 
                                    style={ _styles.tweetImageStyle }/>
                        <RX.View style={ _styles.tweetDescriptionContainer }>
                            <RX.View style={ _styles.profilePictureContainer }>
                                <RX.Image source={ twitter_status_item
                                        .user[TwitterUserKeysValueObject.PROFILE_IMAGE] } 
                                        style={ _styles.profilePictureStyles }/>
                            </RX.View>
                            <RX.View style={ {flex: 1} }>
                                <RX.View style = { _styles.userNameContainerStyles }>
                                    <RX.Text style={ _styles.userTwitterName }>
                                        { twitter_status_item.user['name'] }
                                    </RX.Text>
                                    <RXImageSvg viewBox = { TrackFiendIcons.icons.twitter.viewBox } 
                                                height= { 20 } width={ 20 }
                                                style = { _styles.twitterIconStyles }>
                                        <RXSvgPath d={ TrackFiendIcons.icons.twitter.path } 
                                                    fillColor = { TrackFiendStyles.color.twitterIconColor }
                                                    strokeColor = { TrackFiendStyles.color.twitterIconColor }/>
                                    </RXImageSvg>
                                </RX.View>
                                <RX.Text style={ _styles.userTwitterUserHandle }>
                                    { '@'+twitter_status_item.user['screen_name'] }
                                </RX.Text>
                                <RX.Text style={ _styles.tweetDescriptionStyles }
                                            numberOfLines= { 3 }>
                                    { twitter_status_item.tweet_text.replace('&amp;', '&') }
                                </RX.Text>
                            </RX.View>
                        </RX.View>
                    </RX.View>
                )
            }
        else if(twitter_status_item.has_media!= null && twitter_status_item.has_media 
            && twitter_status_item.media_type == 'video'){
                return(
                    <RX.View style={ _styles.tweetMediaContainer }
                                onPress={ this.playVideo }
                                disableTouchOpacityAnimation={ true }>
                        <RX.View style={ {height: 200} }>
                            <RXVideo source={ twitter_status_item.media_url_https }
                                        style={ _styles.videoStyles }
                                        resizeMode={ 'contain' }
                                        showControls={ false }
                                        preload={ 'auto' }
                                        ref = { (video: RXVideo)=>{ this._videoComponent = video } }/>
                        </RX.View>
                        <RX.View style={ _styles.tweetDescriptionContainer }>
                            <RX.View style={ _styles.profilePictureContainer }>
                                <RX.Image source={ twitter_status_item
                                        .user[TwitterUserKeysValueObject.PROFILE_IMAGE] } 
                                        style={ _styles.profilePictureStyles }/>
                            </RX.View>
                            <RX.View style={ {flex: 1} }>
                                <RX.View style = { _styles.userNameContainerStyles }>
                                    <RX.Text style={ _styles.userTwitterName }>
                                        { twitter_status_item.user['name'] }
                                    </RX.Text>
                                    <RXImageSvg viewBox = { TrackFiendIcons.icons.twitter.viewBox } 
                                                height= { 20 } width={ 20 }
                                                style = { _styles.twitterIconStyles }>
                                        <RXSvgPath d={ TrackFiendIcons.icons.twitter.path } 
                                                    fillColor = { '#00aced' }
                                                    strokeColor = { '#00aced' }/>
                                    </RXImageSvg>
                                </RX.View>
                                <RX.Text style={ _styles.userTwitterUserHandle }>
                                    { '@'+twitter_status_item.user['screen_name'] }
                                </RX.Text>
                                <RX.Text style={ _styles.tweetDescriptionStyles }
                                            numberOfLines= { 3 }>
                                    { twitter_status_item.tweet_text.replace('&amp;', '&') }
                                </RX.Text>
                            </RX.View>
                        </RX.View>
                    </RX.View>
                )
            }
        else{
            return (
                <RX.View style={ _styles.tweetTextContainer }>
                    <RX.View style={ _styles.profilePictureContainer }>
                                <RX.Image source={ twitter_status_item
                                        .user[TwitterUserKeysValueObject.PROFILE_IMAGE] } 
                                        style={ _styles.profilePictureStyles }/>
                    </RX.View>
                    <RX.View style={ {flex: 1} }>
                        <RX.View style = { _styles.userNameContainerStyles }>
                            <RX.Text style={ _styles.userTwitterName }>
                                { twitter_status_item.user['name'] }
                            </RX.Text>
                            <RXImageSvg viewBox = { TrackFiendIcons.icons.twitter.viewBox } 
                                        height= { 20 } width={ 20 }
                                        style = { _styles.twitterIconStyles }>
                                <RXSvgPath d={ TrackFiendIcons.icons.twitter.path } 
                                            fillColor = { '#00aced' }
                                            strokeColor = { '#00aced' }/>
                            </RXImageSvg>
                        </RX.View>
                        <RX.Text style={ _styles.userTwitterUserHandle }>
                            { '@'+twitter_status_item.user['screen_name'] }
                        </RX.Text>
                        <RX.Text style={ _styles.tweetTextStyles }
                                    numberOfLines= { 3 }>
                            { twitter_status_item.tweet_text.replace('&amp;', '&') }
                        </RX.Text>
                    </RX.View>
                </RX.View>
            )
        }
    }
}

export = TwitterTextNewsComponent;