import { ComponentBase } from 'resub'
import { VirtualListView, VirtualListViewItemInfo } from 'reactxp-virtuallistview'
import RX = require('reactxp')
import TrackFiendStyles = require ('../../TrackFiendStyles')
import NewsItem = require ('../../request_clients/models/NewsItem')
import NewsStore from '../../stores/news/NewsStore'
import NewsItemComponent = require('./NewsItemComponent')
import JumboComponent = require('./JumboComponent')
import TwitterTextNewsComponent = require('./TwitterNewsComponent')
import VirtualNewsListView = require('../../state/VirtualNewsListView');
import NewsState = require('../../state/NewsState')
import NavigationStore from '../../stores/navigation/NavigationStore'
import ExternalVideoComponent = require('../video/ExternalVideoComponent');
import Carousel = require('../carousel/Carousel');

const _newsComponentStyles = {
    activityStyles: RX.Styles.createViewStyle({
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
    }),

    spinnerContainer: RX.Styles.createViewStyle({
        flex: 1,
        alignSelf: 'stretch',
        alignItems: 'center',
        justifyContent: 'center'
    }),

    listScroll: RX.Styles.createViewStyle({
        flexDirection: 'column',
        alignSelf: 'stretch',
        backgroundColor: TrackFiendStyles.color.listBackgroundColour,
        paddingTop: 5
    }),

    rootView: RX.Styles.createViewStyle({
        flexDirection: 'column',
        alignSelf: 'stretch'
    })
}

class NewsComponent extends ComponentBase<{}, NewsState>{
    protected _buildState(props:{}, initialBuild: boolean): NewsState{
        return{
            news: NewsStore.getNewsItems(),
            isLoadingNews: NewsStore.isLoading(),
            virtualListNews: NewsStore.getNewsItems().map((item, index)=>{
                return {
                    index: index,
                    key: item.url_to_story + item.title,
                    height: this.getItemHeight(item, index),
                    template: item.item_type,
                    newsItem: item
                }
            })
        }
    }

    getItemHeight(item: NewsItem, index: number): number{
        if(item.item_type == 'article' && index == 0){
            return 290;
        }
        else if(item.item_type == 'twitter_post' && item.has_media){
            return 290;
        }
        else if(item.item_type == 'external_video'){
            return RX.Platform.getType() == 'web'? 205: 225;
        }
        else{
            return 105
        }
    }

    componentDidMount(){
        NewsStore.fetchNewsItems(this.onFetchNewsError);
    }

    onFetchNewsError = ()=>{
        NavigationStore.setRoutePanel('Login', {})
    }

    render(): JSX.Element | null{
        return (
                this.renderNews() 
        );
    }

    renderNews = ()=>{
        if(this.state.isLoadingNews){
            return  <RX.View style={ _newsComponentStyles.spinnerContainer }>
                        <RX.ActivityIndicator color={ TrackFiendStyles.color.mainAppColor } size={ 'medium' } style={ _newsComponentStyles.activityStyles }/>
                    </RX.View>
        }
        else{
            return(
                <VirtualListView itemList = { this.state.virtualListNews }
                                renderItem= { this.renderNewsItems }
                                animateChanges= { true }
                                style={ _newsComponentStyles.listScroll }
                                skipRenderIfItemUnchanged={ false }/>
            )
        }
    }

    onScrollBeginDrag = () =>{

    }

    onScrollEndDrag = () => {

    }

    renderNewsItems = (item: VirtualNewsListView, hasFocus?: boolean)=>{
        if(item.newsItem.item_type == 'article' && item.index == 0){
            return (<JumboComponent key={ item.newsItem.title }
                                                newsItem={ item.newsItem }/>)            
        }
        else if(item.newsItem.item_type == 'twitter_post'){
            return (< TwitterTextNewsComponent key={ item.newsItem.title } 
                                            newsItem={ item.newsItem }/>)
        }
        else if(item.newsItem.item_type == 'external_video'){
            return(<ExternalVideoComponent newsItem={ item.newsItem }/>)
        }
        else{
            return <NewsItemComponent key={ item.newsItem.title } 
                                        newsItem= { item.newsItem}/>
        }
    }
}

export = NewsComponent;