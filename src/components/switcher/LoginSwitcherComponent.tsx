import { ComponentBase } from 'resub'
import RX = require('reactxp')
import { default as RXImageSvg, SvgPath as RXSvgPath } from 'reactxp-imagesvg';
import LoginStore from '../../stores/login/LoginStore'
import TrackFiendStyles = require('../../TrackFiendStyles')
import TrackFiendIcons = require('../../TrackFiendIcons')

interface LoginSwitcherProps{
    onLoginTapped: () => void
    onProfileTapped: () => void
}

interface LoginSwitcherState{
    isLoggedin: boolean
}

const _styles = {
    loginSwitcherContainerStyles: RX.Styles.createViewStyle({
        position: 'absolute',
        right: 0,
        height: 44
    }),

    buttonStyles: RX.Styles.createButtonStyle({
        height: 44,
        alignItems: 'center',
        justifyContent: 'flex-start',
        width: 50
    }),

    loginTextStyles: RX.Styles.createTextStyle({
        color: TrackFiendStyles.color.titleTextColor,
        marginTop: 15

    }),

    profileButtonStyles: RX.Styles.createViewStyle({
        marginTop: 12
    })
}

class LoginSwitcherComponent extends ComponentBase<LoginSwitcherProps, LoginSwitcherState>{
    protected _buildState(props:{}, initialBuild: boolean): LoginSwitcherState{
        return {
            isLoggedin: LoginStore.isLoggedIn()
        }
    }

    render(): JSX.Element | null{
        return (
            <RX.View style={ _styles.loginSwitcherContainerStyles }>
                { this._renderState()}
            </RX.View>
        )
    }

    _renderState = ()=>{
        if(this.state.isLoggedin){
            return (
                <RX.Button style={ _styles.buttonStyles } onPress={ this.props.onProfileTapped }>
                    <RXImageSvg viewBox={ TrackFiendIcons.icons.person.viewBox } 
                                height= { 20 } width={ 20 }
                                style={ _styles.profileButtonStyles }>
                        <RXSvgPath d={ TrackFiendIcons.icons.person.path }
                            fillColor ={ TrackFiendStyles.color.mainAppColor }
                            strokeColor = { TrackFiendStyles.color.titleTextColor } strokeWidth={ 2 }/>
                    </RXImageSvg>
                </RX.Button>
            )
        }
        else{
            return(
                <RX.Button style={ _styles.buttonStyles } onPress={ this.props.onLoginTapped }>
                    <RX.Text style={ [TrackFiendStyles.styles.defaultTextStyles, 
                            _styles.loginTextStyles] }>
                        Log in
                    </RX.Text>
                </RX.Button>
            )
        }
    }
}

export = LoginSwitcherComponent;