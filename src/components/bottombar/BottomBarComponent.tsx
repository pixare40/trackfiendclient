import { ComponentBase } from 'resub'
import RX = require('reactxp')
import TrackFiendStyles = require ('../../TrackFiendStyles')
import TrackFiendIcons = require ('../../TrackFiendIcons')
import TabButtonModel = require('../../models/TabButtonModel')
import TabComponent = require('../tab/TabComponent')
import MenuStore from '../../stores/menu/MenuStore'

const _styles ={
    textColor: RX.Styles.createTextStyle({
        color: TrackFiendStyles.color.white,
        textAlignVertical: 'center',
        lineHeight: 44
    }),

    componentStyles : RX.Styles.createViewStyle({
        height: 44,
        alignSelf: 'stretch',
        justifyContent: 'center',
        borderStyle: 'solid',
        borderColor: TrackFiendStyles.color.mainAppColor,
        borderTopWidth: 1,
        backgroundColor: TrackFiendStyles.color.white,
        flex: 1,
        flexDirection: 'column'
    })
}

interface BottomBarState{
    buttons: TabButtonModel[]
}

interface BottomBarProps{
    onBottomButtonPressed: (tab: string) => void
}

const buttons = [
                {
                    title: 'News',
                    iconPath: TrackFiendIcons.icons.home.path,
                    viewBox: TrackFiendIcons.icons.home.viewBox,
                    isSelected: true,
                    height: 20,
                    width: 20,
                    showIcon: true,
                    textContent: '',
                    menuKey: 'bottombarmenu'
                },
                {
                    title: 'Search',
                    iconPath: TrackFiendIcons.icons.maginifying_glass.path,
                    viewBox: TrackFiendIcons.icons.maginifying_glass.viewBox,
                    isSelected: false,
                    height: 20,
                    width: 20,
                    showIcon: true,
                    textContent: '',
                    menuKey: 'bottombarmenu'
                },
                {
                    title: 'Trending',
                    iconPath: TrackFiendIcons.icons.fire.path,
                    viewBox: TrackFiendIcons.icons.fire.viewBox,
                    isSelected: false,
                    height: 20,
                    width: 20,
                    showIcon: true,
                    textContent: '',
                    menuKey: 'bottombarmenu'
                },
                {
                    title: 'Channels',
                    iconPath: TrackFiendIcons.icons.video.path,
                    viewBox: TrackFiendIcons.icons.video.viewBox,
                    isSelected: false,
                    height: 20,
                    width: 20,
                    showIcon: true,
                    textContent: '',
                    menuKey: 'bottombarmenu'
                },
                {
                    title: 'Menu',
                    iconPath: TrackFiendIcons.icons.menu.path,
                    viewBox: TrackFiendIcons.icons.menu.viewBox,
                    isSelected: false,
                    height: 20,
                    width: 20,
                    showIcon: true,
                    textContent: '',
                    menuKey: 'bottombarmenu'
                }
                
            ]

class BottomBarComponent extends ComponentBase<BottomBarProps, BottomBarState>{
    protected _buildState(props: BottomBarProps, initialBuild:boolean): BottomBarState{
        return {
            buttons: MenuStore.getSelectedMenu('bottombarmenu')
        }
    }

    render(): JSX.Element | null{
        MenuStore.setMenu('bottombarmenu', buttons)
        return(
            <RX.View style= { _styles.componentStyles }>
                <TabComponent buttonList = {buttons} 
                                onPressButton = { this.onTabPressed }/>
            </RX.View>
        );
    }

    private onTabPressed = (tab: string) => {
        let selectedButtonsArray = buttons.map((x)=>{
            if(x.title == tab){
                x.isSelected = true
            }
            else{
                x.isSelected = false
            }
            return x
        })

        MenuStore.setSelectedMenu('bottombarmenu', tab)
        this.props.onBottomButtonPressed(tab);
    }
}

export = BottomBarComponent;