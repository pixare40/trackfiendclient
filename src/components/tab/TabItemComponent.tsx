import { ComponentBase } from 'resub'
import { default as RXImageSvg, SvgPath as RXSvgPath } from 'reactxp-imagesvg';
import RX = require('reactxp')
import MenuStore from '../../stores/menu/MenuStore'
import TrackFiendStyles = require('../../TrackFiendStyles')
import TabButtonModel = require('../../models/TabButtonModel')

interface TabItemProps{
    key: string
    title: string
    iconPath: string
    height: number
    width: number
    isSelected: boolean
    viewBox: string
    onPress: (tab: string) => void
    showIcon: boolean,
    textContent: string,
    menuKey: string,
    textStyles?: RX.Types.TextStyleRuleSet
}

interface TabItemState{
    menu: TabButtonModel[];
}

const styles={
    buttonStyles : RX.Styles.createButtonStyle({
        height: 44,
        alignItems: 'center',
        flex: 1,
        justifyContent: 'flex-start'
    }),

    viewStyles: RX.Styles.createViewStyle({
       paddingLeft: 10,
       paddingRight: 10,
       flex: 0.5
    }),

    svgStyles: RX.Styles.createViewStyle({
        marginTop: 12
    })
}

class TabItemComponent extends ComponentBase<TabItemProps, TabItemState>{
    protected _buildState(props: TabItemProps, initialBuild: boolean): TabItemState{
        return {
            menu: MenuStore.getSelectedMenu(this.props.menuKey)
        }
    }

    //TODO [ Extremely Ugly ] Should make comparison before force updating component to improve perf
    shouldComponentUpdate(nextProps: TabItemProps, nextState: TabItemState){
        return true;
    }

    render() : JSX.Element | null{
        return (
            <RX.View style={ styles.viewStyles }>
                <RX.Button onPress = { this.onTabPressed }
                        style={ styles.buttonStyles }>
                    { this.renderButton() }
                </RX.Button>
            </RX.View>
        )
    }

    private renderButton = () =>{
        if(this.props.showIcon){
            return (
                <RXImageSvg viewBox = { this.props.viewBox } height={ this.props.height } 
                                width={ this.props.width } style={ styles.svgStyles } >

                    <RXSvgPath d= { this.props.iconPath }
                                fillColor = {this.isSelected()}
                                strokeColor = { TrackFiendStyles.color.mainAppColor }/>

                </RXImageSvg>
            )
        }
        else{
            return(
                <RX.Text style={ this.props.textStyles } >
                    { this.props.textContent }
                </RX.Text>
            )
        }
    }

    private onTabPressed = () => {
        this.props.onPress(this.props.title)
    }

    private isSelected = () => {
        let selected: boolean
        this.state.menu.forEach(item=>{
            if((item.title == this.props.title) && item.isSelected){
                selected = true;
            }
        })

        if(selected){
            return TrackFiendStyles.color.mainAppColor
        }
        else{
            return TrackFiendStyles.color.defaultTabDeselectedColor
        }
    }
}

export = TabItemComponent;