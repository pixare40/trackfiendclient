import { ComponentBase } from 'resub'
import RX = require('reactxp')
import TabButtonModel = require('../../models/TabButtonModel')
import TabItemComponent = require('./TabItemComponent')

interface TabComponentProps{
    buttonList: Array<TabButtonModel>,
    onPressButton: (tab: string)=> void
}

const _styles = {
    tabComponentStyles : RX.Styles.createViewStyle({
        flexDirection: 'row',
        flex: 1
    })
}

class TabComponent extends ComponentBase<TabComponentProps, null>{

    render(): JSX.Element | null{
        return (
            <RX.View style = {_styles.tabComponentStyles}>
                { this.renderTabButtons() }
            </RX.View>
        )
    }

    private renderTabButtons(){
        return (
            this.props.buttonList.map((button)=>{
                return <TabItemComponent key={ button.title } title = {button.title}
                                    height = {button.height}
                                    width = { button.width }
                                    iconPath = { button.iconPath }
                                    isSelected = { button.isSelected }
                                    onPress = { this.props.onPressButton }
                                    viewBox = { button.viewBox }
                                    showIcon = { button.showIcon }
                                    textContent = { button.textContent }
                                    menuKey = { button.menuKey }
                                    textStyles = { button.textStyles }/>
            })
        )
    }
}

export = TabComponent;