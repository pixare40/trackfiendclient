import { ComponentBase } from 'resub'
import { VirtualListView, VirtualListViewItemInfo } from 'reactxp-virtuallistview'
import RX = require('reactxp')
import UserProfileStore from '../../stores/profile/UserProfileStore'
import UserInfoModel = require('../../models/user/UserInfoModel')
import TrackFiendStyles = require('../../TrackFiendStyles')
import TabButtonModel = require('../../models/TabButtonModel')
import TabComponent = require('../tab/TabComponent')
import MenuStore from '../../stores/menu/MenuStore'
import ArtistInfoModel = require('../../models/artists/ArtistInfoModel')
import ArtistListItemComponent = require('../search/ArtistListItemComponent')
import NavigationStore from '../../stores/navigation/NavigationStore'

interface ProfileComponentState{
    isFetchingUserInfo: boolean
    userInfo: UserInfoModel
    menuItems: TabButtonModel[]

}

interface ArtistResultsList extends VirtualListViewItemInfo{
    artistinfo: ArtistInfoModel
}

const _styles = {
    rootComponent: RX.Styles.createViewStyle({
        flex: 1
    }),

    profileComponent: RX.Styles.createViewStyle({
        backgroundColor: TrackFiendStyles.color.profileHeaderBackgroundColor,
        height: 120,
        justifyContent: 'center',
        alignItems: 'center'
    }),

    profileComponentText: RX.Styles.createTextStyle({
        color: TrackFiendStyles.color.profileHeaderTextColor,
        fontWeight: 'bold',
        fontSize: 16
    }),

    tabContainerStyles: RX.Styles.createViewStyle({
        maxHeight: 45,
        borderBottomWidth: 1,
        borderColor: TrackFiendStyles.color.mainAppColor,
        borderStyle: 'solid'
    }),

    virtualListView: RX.Styles.createViewStyle({
        backgroundColor: TrackFiendStyles.color.listBackgroundColour
    }),

    tabContentContainerStyles: RX.Styles.createViewStyle({
        flex: 1,
        alignItems: 'stretch',
        marginLeft: 5,
        marginRight: 5
    })
}

const buttons: TabButtonModel[] = [
    {
        title: 'Artists Followed',
        isSelected: true,
        showIcon: false,
        menuKey: 'profilemenu',
        textContent: 'Artists Followed',
        textStyles: RX.Styles.createTextStyle({
            marginTop: 16
        })
    },
    {
        title: 'Comments',
        isSelected: false,
        showIcon: false,
        menuKey: 'profilemenu',
        textContent: 'Comments',
        textStyles: RX.Styles.createTextStyle({
            marginTop: 16
        })
    }
]

class ProfileComponent extends ComponentBase<{}, ProfileComponentState>{
    protected _buildState(props: {}, initialBuild: false): ProfileComponentState{
        return {
            userInfo: UserProfileStore.getUserInfo(),
            isFetchingUserInfo: UserProfileStore.getIsFetchingUserInfo(),
            menuItems: MenuStore.getSelectedMenu('profilemenu')
        }
    }

    //TODO [ Extremely Ugly ] Should make comparison before force updating component to improve perf
    shouldComponentUpdate(nextProps: {}, nextState: ProfileComponentState){
        return true;
    }

    componentDidMount(){
        UserProfileStore.fetchUserInfo()
    }

    render(): JSX.Element | null{
        MenuStore.setMenu('profilemenu', buttons)
        if(this.state.isFetchingUserInfo){
            return(
                <RX.View style={ TrackFiendStyles.styles.spinnerContainer }>
                    <RX.ActivityIndicator color= { TrackFiendStyles.color.mainAppColor } size={ 'medium' }/>
                </RX.View>
            )
        }
        
        return (
            <RX.View style={ _styles.rootComponent }>
                <RX.View style={ _styles.profileComponent }>
                    <RX.Text style={ _styles.profileComponentText }>
                        { this.state.userInfo.firstname } { this.state.userInfo.lastname }
                    </RX.Text>
                </RX.View>
                <RX.View style={ _styles.tabContainerStyles }>
                    <TabComponent buttonList = { buttons } 
                                onPressButton={ this.onTabButtonPressed }/>
                </RX.View>
                <RX.View style={ _styles.tabContentContainerStyles }>
                    { this.renderTabContents() }
                </RX.View>
            </RX.View>
        )
    }

    onTabButtonPressed = (tab: string) =>{
        MenuStore.setSelectedMenu('profilemenu', tab);
    }

    renderTabContents = ()=>{
        let selectedMenu: string;
        let menuToLoop = this.state.menuItems? this.state.menuItems: buttons;
        menuToLoop.forEach((item)=>{
            if(item.isSelected){
                selectedMenu = item.title
            }
        })

        if(selectedMenu == 'Artists Followed'){
            let artistListInfoList: ArtistResultsList[] = []
            UserProfileStore.retrieveFollowingArtists().forEach(item=>{
                let artistListInfo: ArtistResultsList = {
                    artistinfo: item,
                    key: item.name,
                    template: 'Artist',
                    height: 44
                }

                artistListInfoList.push(artistListInfo);
            })

            return (
                <VirtualListView itemList={ artistListInfoList }
                                    renderItem={ this.renderArtistList }
                                    style={ _styles.virtualListView }/>
            )
        }
        else if(selectedMenu == 'Comments'){
            return (
                <RX.View>
                    <RX.Text>
                        Comments Section
                    </RX.Text>
                </RX.View>
            )
        }else{
            return (
                <RX.View>
                    <RX.Text>
                        Something wrong happened
                    </RX.Text>
                </RX.View>
            )
        }
    }

    renderArtistList = (item: ArtistResultsList, hasFocus?:boolean) => {
        return (
                <ArtistListItemComponent key={ item.artistinfo.name } 
                                artist={ item.artistinfo } 
                                onArtistTapped={ this.goToArtist }/>
        )
    }

    goToArtist = (artistId: number) =>{
        NavigationStore.setRoutePanel('ArtistInfo', {'artist_id': artistId})
    }
}

export = ProfileComponent;