import { ComponentBase } from'resub'
import RX = require('reactxp')
import LoginStore from '../../stores/login/LoginStore'
import TrackFiendStyles = require('../../TrackFiendStyles')
import UserProfileStore from '../../stores/profile/UserProfileStore'
import NavigationStore from '../../stores/navigation/NavigationStore'
//const FBSDK = require('react-native-fbsdk')
//const { LoginButton, } = FBSDK

interface LoginState{
    loggedIn: boolean,
    username: string,
    password: string,
    firstname: string,
    lastname: string
}

const _styles ={
    rootViewStyles: RX.Styles.createViewStyle({
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'stretch',
        flex: 1,
    }),

    inputContainerStyles: RX.Styles.createViewStyle({
        alignSelf: 'center',
        width: 250
    }),

    inputBoxStyles: RX.Styles.createTextInputStyle({
        borderStyle: 'solid',
        borderBottomWidth: 1,
        borderColor: TrackFiendStyles.color.mainAppColor,
        height: 40,
        margin: 2,
        textAlign: 'justify',
        borderRadius: 0
    }),
    
    buttonContainer: RX.Styles.createViewStyle({
        marginTop: 20,
        alignItems: 'center',
        justifyContent: 'flex-start',
    }),

    authButtonsStyles: RX.Styles.createButtonStyle({
        height: 40,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: TrackFiendStyles.color.mainAppColor,
        borderRadius: 20,
        margin: 2,
        alignSelf: 'stretch'
    }),

    loginWithFacebookbuttonStyles: RX.Styles.createButtonStyle({
        backgroundColor: TrackFiendStyles.color.facebookColor
    }),

    authButtonTextStyles: RX.Styles.createTextStyle({
        textAlignVertical: 'center',
        alignSelf: 'center',
        color: TrackFiendStyles.color.titleTextColor,
        marginTop: 13
    }),

    loginContainerModalStyles: RX.Styles.createViewStyle({
        position: 'absolute'
    }),

    registrationContainer: RX.Styles.createViewStyle({
        flexDirection: 'row',
        flex: 1,
        flexWrap: 'nowrap',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 10,
    }),

    registrationText: RX.Styles.createTextStyle({
        alignSelf: 'auto',
        flex: 1,
        fontSize: 12,
        color: TrackFiendStyles.color.mainAppColor,
    }),

    secondaryRegistrationText: RX.Styles.createTextStyle({
        alignSelf: 'auto',
        flex: 1,
        fontSize: 12,
        textAlign: 'right',
        color: TrackFiendStyles.color.mainAppColor,
    })
}

class LoginComponent extends ComponentBase<{}, LoginState>{
    protected _buildState(props: {}, initialBuild: boolean): LoginState{
        return{
            loggedIn : LoginStore.isLoggedIn(),
            username: '',
            password: '',
            firstname: '',
            lastname: ''
        }
    }

    render() : JSX.Element | null{
        return (
            <RX.View style={ _styles.rootViewStyles }>
                { this._renderLoginPanel() }
            </RX.View>
        )
    }

    componentDidUpdate(){
        if(this.state.loggedIn){
            UserProfileStore.fetchUserInfo()
            NavigationStore.setRoutePanel('News', {})
        }
    }

    _renderLoginPanel = ()=>{
        return (
            <RX.View style={ _styles.inputContainerStyles }>
                    <RX.TextInput style={ _styles.inputBoxStyles } 
                                    placeholder={ 'Email' } 
                                    textAlign={ 'justify' } 
                                    onChangeText = { (text) =>{ this.state.username = text } }
                                    keyboardType={ 'email-address' }></RX.TextInput>

                    <RX.TextInput style={ _styles.inputBoxStyles } 
                                    placeholder={ 'Password' }
                                    textAlign = { 'justify' }
                                    onChangeText={ (textVal)=>{ this.state.password = textVal } }
                                    secureTextEntry={ true }></RX.TextInput>

                    <RX.View style={ _styles.buttonContainer }>
                        <RX.Button style={ _styles.authButtonsStyles } onPress= { this.onLoginPressed }>
                            <RX.Text style={ _styles.authButtonTextStyles }>
                                Login
                            </RX.Text>
                        </RX.Button>
                        
                        { this.renderLoginButton() }

                    </RX.View>
                    <RX.View style={ _styles.registrationContainer }>
                        <RX.Text style={ _styles.registrationText } 
                                    onPress={ this.onRegistrationTapped }>
                            { 'Create Account' }
                        </RX.Text>
                        <RX.Text style={ _styles.secondaryRegistrationText }>
                            { 'Terms & Conditions' }
                        </RX.Text>
                    </RX.View>
            </RX.View>
        )
    }
    
    onLoginPressed = ()=> {
        LoginStore.login(this.state.username, this.state.password)
    }

    onRegistrationTapped = () =>{
        NavigationStore.setRoutePanel('Registration', {});
    }

    renderLoginButton = () =>{
        //if(RX.Platform.getType() == 'web'){
            return (
                <RX.Button style={ [_styles.authButtonsStyles, _styles.loginWithFacebookbuttonStyles] }>
                    <RX.Text style={ _styles.authButtonTextStyles }>
                        Login with Facebook
                    </RX.Text>
                </RX.Button>
            )
        //}

        // return(
        //     <LoginButton 
        //         publishPermissions={ ['email','public_profile'] }
        //         onLoginFinished = { this.onLoginCompleted }
        //         onLogoutFinished = { ()=>{} }
        //     />
        // )
    }

    onLoginCompleted = (error: any, response: any)=>{

    }
}

export = LoginComponent;