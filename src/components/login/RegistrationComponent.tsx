import { ComponentBase } from 'resub'
import RX = require('reactxp')
import TrackFiendStyles = require('../../TrackFiendStyles')
import LoginClient from '../../request_clients/LoginClient'
import NavigationStore from '../../stores/navigation/NavigationStore'

interface RegistrationState{
    firstName: string,
    lastName: string,
    password: string,
    email: string
}

const _styles ={
    inputTextStyles: RX.Styles.createTextInputStyle({
        borderStyle: 'solid',
        borderBottomWidth: 1,
        borderColor: TrackFiendStyles.color.mainAppColor,
        height: 40,
        margin: 2,
        textAlign: 'justify',
        borderRadius: 0
    }),

    registrationInputStyles: RX.Styles.createViewStyle({
        justifyContent: 'center',
        flex: 1,
        alignItems: 'center',
        alignSelf: 'stretch'
    }),

    authButtonTextStyles: RX.Styles.createTextStyle({
        textAlignVertical: 'center',
        alignSelf: 'center',
        color: TrackFiendStyles.color.titleTextColor,
        marginTop: 13
    }),

    buttonContainer: RX.Styles.createViewStyle({
        marginTop: 20,
        alignItems: 'center',
        justifyContent: 'flex-start',
    }),

    inputContainerStyles: RX.Styles.createViewStyle({
        alignSelf: 'center',
        width: 250
    }),
}

class RegistrationComponent extends ComponentBase<{}, RegistrationState>{
    protected _buildState(props:{}, initialBuild: boolean): RegistrationState{
        return {
            firstName: '',
            lastName: '',
            password: '',
            email: ''
        };
    }

    render(): JSX.Element | null{
        return (
            <RX.View style={ _styles.registrationInputStyles }>
                <RX.View style={ _styles.inputContainerStyles }>
                    <RX.TextInput placeholder={ 'First Name' }
                                    textAlign={ 'justify' }
                                    style={ _styles.inputTextStyles }
                                    onChangeText = { (textVal)=>{ this.state.firstName = textVal } } />
                    <RX.TextInput placeholder={ 'Last Name' }
                                    textAlign={ 'justify' }
                                    style={ _styles.inputTextStyles }
                                    onChangeText={ (textVal)=>{ this.state.lastName = textVal } } />
                    <RX.TextInput placeholder={ 'Email' }
                                    style={ _styles.inputTextStyles }
                                    textAlign={ 'justify' }
                                    onChangeText={ (textVal)=>{ this.state.email = textVal } } />
                    <RX.TextInput placeholder={ 'Password' }
                                    style={ _styles.inputTextStyles }
                                    textAlign={ 'justify' }
                                    secureTextEntry={ true }
                                    onChangeText={ (textVal)=>{ this.state.password = textVal } } />
                    <RX.View style={ _styles.buttonContainer }>
                        <RX.Button style={ TrackFiendStyles.styles.defaultRoundedButtonStyles } onPress= { this.onRegisterTapped }>
                            <RX.Text style={ _styles.authButtonTextStyles }>
                                Register
                            </RX.Text>
                        </RX.Button>

                        <RX.Button style={ [TrackFiendStyles.styles.defaultRoundedButtonStyles, 
                                            TrackFiendStyles.styles.facebookButtonStyles] }>
                            <RX.Text style={ _styles.authButtonTextStyles }>
                                Register with Facebook
                            </RX.Text>
                        </RX.Button>
                    </RX.View>
                </RX.View>
            </RX.View>
        )
    }

    onRegisterTapped = ()=>{
        LoginClient.register(this.state.email, 
                                this.state.password, 
                                this.state.firstName, 
                                this.state.lastName).then(response=>{
            NavigationStore.setRoutePanel('Login', null)
        })
    }
}

export = RegistrationComponent;