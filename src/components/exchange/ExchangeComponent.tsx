import { ComponentBase } from 'resub'
import RX = require('reactxp')

interface ExchangeState{
    title: string
}

interface ExchangeComponentProps{
}

class ExchangeComponent extends ComponentBase<ExchangeComponentProps, ExchangeState>{
    protected _buildState(props:{}, initialBuild: boolean): ExchangeState{
        return {
            title: 'Exchange Page'
        }
    }

    render(): JSX.Element | null{
        return (
            <RX.View>
                <RX.Text>
                    {this.state.title}
                </RX.Text>
            </RX.View>
        )
    }

}

export = ExchangeComponent;