import { ComponentBase } from 'resub'
import TrackArtistClient from '../../request_clients/TrackArtistClient'
import RX = require('reactxp')
import ArtistSearchStore from '../../stores/search/ArtistSearchStore'
import ArtistInfoModel = require('../../models/artists/ArtistInfoModel')
import TrackFiendStyles = require('../../TrackFiendStyles')
import LoginStore from '../../stores/login/LoginStore'
import UserProfileStore from '../../stores/profile/UserProfileStore'
import UserInfoModel = require('../../models/user/UserInfoModel')

interface ArtistListItemProps{
    key: string
    artist: ArtistInfoModel
    showTracking?: boolean
    showUntrack?: boolean
    onArtistTapped: (artistId: number) => void
}

const _styles = {
    rootViewStyles: RX.Styles.createViewStyle({
        flexDirection: 'row',
        justifyContent: 'center',
        height: 40,
        marginBottom: 2,
        backgroundColor: TrackFiendStyles.color.appBackgroundColour
    }),

    trackButtonStyles: RX.Styles.createButtonStyle({
        borderStyle: 'solid',
        borderColor: TrackFiendStyles.color.mainAppColor,
        borderWidth: 1,
        height: 20,
        alignItems: 'center',
        width: 100,
        borderRadius: 5
    }),

    trackingTextContainerStyle: RX.Styles.createViewStyle({
        width: 100,
        alignItems: 'center'
    }),

    trackTextStyle: RX.Styles.createTextStyle({
        color: TrackFiendStyles.color.mainAppColor,
        fontSize: 11,
        fontWeight: 'bold',
    }),

    artistNameContainerStyles: RX.Styles.createViewStyle({
        flex: 1,
        justifyContent: 'center'
    }),

    artistNameStyles: RX.Styles.createTextStyle({
        fontSize: 11,
    }),

    buttonContainerView: RX.Styles.createViewStyle({
        marginRight: 10,
        alignItems: 'center',
        flex: 0,
        justifyContent: 'center'
    })
}

interface ArtistListItemComponentState{
    authToken: string,
    followedArtists: UserInfoModel
}

class ArtistListItemComponent extends ComponentBase<ArtistListItemProps, ArtistListItemComponentState>{
    protected _buildState(props: ArtistListItemProps, initialBuild: false): ArtistListItemComponentState{
        return ({
            authToken: LoginStore.getAuthenticationToken(),
            followedArtists: UserProfileStore.getUserInfo()
        })
    }

    render(): JSX.Element | null{
        return (
            <RX.View style={ _styles.rootViewStyles }>
                <RX.View style={ _styles.artistNameContainerStyles } 
                            onPress = { this.onArtistPressed }>
                    <RX.Text style={ _styles.artistNameStyles }>
                        { this.props.artist.name }
                    </RX.Text>
                </RX.View>
                <RX.View style={ _styles.buttonContainerView }>
                    { this.showTrackingAction() }
                </RX.View>
            </RX.View>
        )
    }

    onArtistPressed = ()=>{
        this.props.onArtistTapped(this.props.artist.id)
    }

    startTrackingArtist = () => {
        TrackArtistClient.trackArtist(this.props.artist.id, this.state.authToken)
        .then(()=>{
            UserProfileStore.fetchUserInfo();
        })
    }

    showTrackingAction = () => {
        if(LoginStore.getLoggedInStatus() && UserProfileStore.isFollowingArtist(this.props.artist.id)){
            return(
                <RX.View style={ _styles.trackingTextContainerStyle }>
                    <RX.Text style={ _styles.trackTextStyle }>
                        Tracking
                    </RX.Text>
                </RX.View>
            )
        }

        return(
            <RX.Button style={ _styles.trackButtonStyles } onPress={ this.startTrackingArtist }>
                <RX.Text style={ _styles.trackTextStyle }>
                    Track
                </RX.Text>
            </RX.Button>
        )

    }
}

export = ArtistListItemComponent;