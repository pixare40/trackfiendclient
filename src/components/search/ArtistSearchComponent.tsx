import { ComponentBase } from 'resub'
import { VirtualListView, VirtualListViewItemInfo } from 'reactxp-virtuallistview'
import RX = require('reactxp')
import ArtistSearchStore from '../../stores/search/ArtistSearchStore'
import ArtistInfoModel = require('../../models/artists/ArtistInfoModel')
import ArtistListItemComponent = require('./ArtistListItemComponent')
import TrackFiendStyles = require('../../TrackFiendStyles')
import TopFollowedArtistsComponent = require('../artist/TopFollowedArtistsComponent')
import NavigationStore from '../../stores/navigation/NavigationStore'
import ArtistStore from '../../stores/artists/ArtistsStore'

interface ArtistSearchComponentState{
    artistNames: ArtistInfoModel[]
    artistListInfo: ArtistSearchResultsList[]
}

interface ArtistSearchResultsList extends VirtualListViewItemInfo{
    artistInfo: ArtistInfoModel
}

const _styles = {
    rootViewStyles: RX.Styles.createViewStyle({
        marginLeft: 5,
        marginRight: 5,
        flex: 1,
        alignItems: 'stretch'
    }),

    searchInputFieldStyles: RX.Styles.createTextInputStyle({
        borderStyle: 'solid',
        borderBottomWidth: 1,
        borderColor: TrackFiendStyles.color.mainAppColor,
        height: 44,
        margin: 2,
        textAlign: 'justify',
        borderRadius: 0
    }),

    scrollViewStyles: RX.Styles.createScrollViewStyle({
        backgroundColor: TrackFiendStyles.color.listBackgroundColour
    })
}

class ArtistSearchComponent extends ComponentBase<{}, ArtistSearchComponentState>{
    protected _buildState(props: {}, initialBuild: false): ArtistSearchComponentState{
        return {
            artistNames: ArtistSearchStore.getSearchResults(),
            artistListInfo: ArtistSearchStore.getSearchResults().map((item)=>{
                return {
                    artistInfo: item,
                    key: item.name,
                    template: 'Artist',
                    height: 44
                }
            })
        }
    }

    render(): JSX.Element | null{
        return(
            <RX.View style= { _styles.rootViewStyles }>
                <RX.View>
                    <RX.TextInput style={ _styles.searchInputFieldStyles } 
                                    placeholder={ 'Search' } 
                                    onChangeText= { (searchText) =>{ this.fetchSearchResults(searchText) } } />
                </RX.View>
                { this.renderSearchResultsOrTopFollowed() }
            </RX.View>
        )
    }

    renderSearchResultsOrTopFollowed = ()=>{
        if(this.state.artistListInfo.length == 0){
            return (
                <TopFollowedArtistsComponent/>
            )
        }
        else{
            return (
                <VirtualListView itemList={ this.state.artistListInfo }
                                        renderItem={ this.renderSearchResults }
                                        style= { _styles.scrollViewStyles }/>
            )
        }
        
    }
    
    renderSearchResults = (item: ArtistSearchResultsList, hasFocus?:boolean) => {
        return (
                <ArtistListItemComponent key={ item.artistInfo.name } 
                                    artist={ item.artistInfo } 
                                    onArtistTapped = { this.goToArtist }/>
        )
    }

    fetchSearchResults = (searchText: string) =>{
        if(!searchText){
            return;
        }
        
        ArtistSearchStore.fetchArtistSearchResults(searchText);
    }

    goToArtist = (artistId: number) =>{
        ArtistStore.setCurrentViewedArtist(artistId)
        NavigationStore.setRoutePanel('ArtistInfo', {'artist_id': artistId})
    }
}

export = ArtistSearchComponent;