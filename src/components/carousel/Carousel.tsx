import { ComponentBase } from 'resub';
import RX = require('reactxp');
const _debounce = require('lodash.debounce');
const IS_RTL = RX.International.isRTL();

interface CarouselProps{
}

interface CarouselState{
    firstSlideXValue: RX.Animated.Value;
    secondSlideXValue: RX.Animated.Value;
    thirdSlideXValue: RX.Animated.Value;
    firstSlideToValue: number;
    secondSlideToValue: number;
    thirdStyleToValue: number;
}

const deviceWidth = RX.UserInterface.measureWindow().width
const FIXED_BAR_WIDTH = 280
const BAR_SPACE = 10
const PROMOS_HEIGHT = 150

const images = [
  'https://s-media-cache-ak0.pinimg.com/originals/ee/51/39/ee5139157407967591081ee04723259a.png',
  'https://s-media-cache-ak0.pinimg.com/originals/40/4f/83/404f83e93175630e77bc29b3fe727cbe.jpg',
  'https://s-media-cache-ak0.pinimg.com/originals/8d/1a/da/8d1adab145a2d606c85e339873b9bb0e.jpg',
]

const _styles = {
    container: RX.Styles.createViewStyle({
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: PROMOS_HEIGHT,
        flexDirection: 'row'
    }),

    imageContainer: RX.Styles.createViewStyle({
        height: PROMOS_HEIGHT,
        position: 'absolute'
    }),

    track: RX.Styles.createImageStyle({
        backgroundColor: '#ccc',
        overflow: 'hidden',
        height: 2,
    }),

    imageStyles: RX.Styles.createImageStyle({
        height: PROMOS_HEIGHT,
        width: deviceWidth,
        position: 'absolute'
    }),

    gestureViewStyles: RX.Styles.createViewStyle({
        height: PROMOS_HEIGHT,
        width: deviceWidth
    })
}

class Carousel extends ComponentBase<CarouselProps, CarouselState>{
    fromAnimValue = new RX.Animated.Value(-200)
    slideIn: RX.Types.Animated.CompositeAnimation;
    slideOut: RX.Types.Animated.CompositeAnimation;
    carouselTransitionAnimations: RX.Types.Animated.CompositeAnimation;
    scrollView: RX.View;
    transitionCounter: number;
    initialFirstSlideToValue: number;
    initialSecondSlideToValue: number;
    initialThirdSlideToValue: number;

    constructor(props: CarouselProps){
        super(props);

        this.initialFirstSlideToValue = 0;
        this.initialSecondSlideToValue = deviceWidth;
        this.initialThirdSlideToValue = deviceWidth*2;
        this.transitionCounter = 0;
    }

    componentWillMount(){
        this.setState({
            firstSlideXValue: new RX.Animated.Value(this.initialFirstSlideToValue),
            secondSlideXValue: new RX.Animated.Value(this.initialSecondSlideToValue),
            thirdSlideXValue: new RX.Animated.Value(this.initialThirdSlideToValue),
            firstSlideToValue: -deviceWidth,
            secondSlideToValue: 0,
            thirdStyleToValue: deviceWidth
        })
    }

    componentDidMount(){
        this.startCarouselTransitions();
    }

    startCarouselTransitions = ()=> {
        this.carouselTransitionAnimations = RX.Animated.parallel([
            RX.Animated.timing(
                this.state.firstSlideXValue,{
                    toValue: this.state.firstSlideToValue,
                    duration: 175,
                    delay: 3000,
                    easing: RX.Animated.Easing.Linear()
                }
            ),
            RX.Animated.timing(
                this.state.secondSlideXValue, {
                    toValue: this.state.secondSlideToValue,
                    duration: 175,
                    delay: 3000,
                    easing: RX.Animated.Easing.Linear()
                }
            ),
            RX.Animated.timing(
                this.state.thirdSlideXValue, {
                    toValue: this.state.thirdStyleToValue,
                    duration: 175,
                    delay: 3000,
                    easing: RX.Animated.Easing.Linear()
                }
            )
        ])

        this.carouselTransitionAnimations.start(this.transitionsComplete)
    }

    transitionsComplete = () =>{
        this.transitionCounter += 1;
        if(this.transitionCounter < 2){
            this.state.firstSlideToValue -= deviceWidth;
            this.state.secondSlideToValue -= deviceWidth;
            this.state.thirdStyleToValue -= deviceWidth; 
        }
        else{
            this.setState({
                firstSlideXValue: new RX.Animated.Value(this.initialFirstSlideToValue),
                secondSlideXValue: new RX.Animated.Value(this.initialSecondSlideToValue),
                thirdSlideXValue: new RX.Animated.Value(this.initialThirdSlideToValue),
                firstSlideToValue: -deviceWidth,
                secondSlideToValue: 0,
                thirdStyleToValue: deviceWidth
            });
        }

        this.carouselTransitionAnimations.start
    }

    render(): JSX.Element | null{
        let firstSlideStyle = RX.Styles.createAnimatedViewStyle({
            transform: [{
                translateX: this.state.firstSlideXValue
            }],
            width: new RX.Animated.Value(deviceWidth),
            height: new RX.Animated.Value(PROMOS_HEIGHT)
        });

        let secondSlideStyle = RX.Styles.createAnimatedViewStyle({
            transform: [{
                translateX: this.state.secondSlideXValue
            }],
            width: new RX.Animated.Value(deviceWidth),
            height: new RX.Animated.Value(PROMOS_HEIGHT)
        });

        let thirdSlideStyle = RX.Styles.createAnimatedViewStyle({
            transform: [{
                translateX: this.state.thirdSlideXValue,
            }],
            width: new RX.Animated.Value(deviceWidth*2),
            height: new RX.Animated.Value(PROMOS_HEIGHT)
        });

        return(
            <RX.View style={ [_styles.container, {width: deviceWidth*2}] } 
                            ref={(scrollView: RX.View)=>{ this.scrollView = scrollView; }}>
                 <RX.View style={ _styles.imageContainer }>
                    <RX.Animated.View style={ firstSlideStyle } >
                        <RX.Image source={ images[0] } resizeMode={ 'cover' } 
                                    style={ _styles.imageStyles } >
                            <RX.Text>
                                { "First Slide" }
                            </RX.Text>
                        </RX.Image>
                    </RX.Animated.View>
                </RX.View>
                <RX.View style={ _styles.imageContainer }> 
                    <RX.Animated.View style={ secondSlideStyle }>
                        <RX.Image source={ images[1] } resizeMode={ 'cover' } 
                                    style={ _styles.imageStyles } >
                            <RX.Text>
                                { "Second Slide" }
                            </RX.Text>
                        </RX.Image>
                    </RX.Animated.View>
                </RX.View>
                <RX.View style={ _styles.imageContainer }> 
                    <RX.Animated.View style={ thirdSlideStyle }>
                        <RX.Image source={ images[2] } resizeMode={ 'cover' } 
                                    style={ _styles.imageStyles } >
                            <RX.Text>
                                { "Third Slide" }
                            </RX.Text>
                        </RX.Image>
                    </RX.Animated.View>
                </RX.View> 
            </RX.View>
        )
    }

    onScrollEndDrag = () => {

    }

    onScrollBeginDrag = () => {
        console.log("Drag entering");
    }

    onScroll = (scrollTopNumber: number, scrollLeftNumber: number) => {
        console.log("scrolling");
    }

    onPanHorizontal = (gestureState: RX.Types.PanGestureState)=>{
        this.carouselTransitionAnimations.stop();
        let initialXValue = gestureState.initialClientX;
        let currentXValue = gestureState.clientX;
        let difference = initialXValue - currentXValue;
        this.state.firstSlideXValue = new RX.Animated.Value(difference);
        this.state.secondSlideXValue = new RX.Animated.Value(difference);
        if(gestureState.isComplete){
            this.carouselTransitionAnimations.start()
        }
    }
}

export = Carousel;