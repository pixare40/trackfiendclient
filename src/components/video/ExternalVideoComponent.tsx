import { ComponentBase } from 'resub'
import RX = require('reactxp')
import NewsItem = require('../../request_clients/models/NewsItem')
import TrackFiendStyles = require('../../TrackFiendStyles')

interface ExternalVideoComponentProps{
    newsItem: NewsItem
}

const sandboxFeatures = RX.Types.WebViewSandboxMode.AllowForms |
RX.Types.WebViewSandboxMode.AllowScripts |
RX.Types.WebViewSandboxMode.AllowPopups |
RX.Types.WebViewSandboxMode.AllowSameOrigin;

const baseUrl = "http://192.168.1.65:4000/show-video/"
const _style = {
    rootView: RX.Styles.createViewStyle({
        height: RX.Platform.getType() == 'web'? 200: 220,
        marginBottom: 5,
        backgroundColor: TrackFiendStyles.color.appBackgroundColour,
        borderColor: TrackFiendStyles.color.cardBorderColor,
        borderWidth: 1,
        borderStyle: 'solid',
    }),

    webviewStyle: RX.Styles.createWebViewStyle({
        height: RX.Platform.getType() == 'web'? 180: 200,
    }),

    titleTextStyles: RX.Styles.createTextStyle({
        fontSize: 12,
        fontWeight: 'bold',
        height: 20,
        paddingRight: 10,
        paddingLeft: 10
    })
}
class ExternalVideoComponent extends ComponentBase<ExternalVideoComponentProps, null>{
    protected _buildState(props: {}, initialBuild: boolean): null{
        return null;
    }

    render(): JSX.Element | null{
        return (
            <RX.View style={ _style.rootView }>
                <RX.Text style={ _style.titleTextStyles }>
                    { this.props.newsItem.title }
                </RX.Text>
                <RX.WebView sandbox = { sandboxFeatures }
                        scalesPageToFit={ true }
                        javaScriptEnabled={ true }
                        style = { _style.webviewStyle } 
                        url={ this.renderGetUrl() } />
            </RX.View>
        )
    }

    renderGetUrl = () => {
        if(RX.Platform.getType() == 'web'){
            return this.props.newsItem.url_to_story;
        }

        return baseUrl + encodeURIComponent(this.props.newsItem.url_to_story)
    }
}

export = ExternalVideoComponent;