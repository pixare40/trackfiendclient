import { ComponentBase } from 'resub'
import RX = require('reactxp')
import TrackFiendStyles = require('../../TrackFiendStyles')
import TrackFiendIcons = require('../../TrackFiendIcons')
import ViewExternalStore from '../../stores/view_external/ViewExternalStore'
import NavigationStore from '../../stores/navigation/NavigationStore'
import { default as RXImageSvg, SvgPath as RXSvgPath } from 'reactxp-imagesvg';

interface ViewExternalComponentState{
    url: string,
    isLoadingExternalUrl: boolean
}

const sandboxFeatures = RX.Types.WebViewSandboxMode.AllowForms |
RX.Types.WebViewSandboxMode.AllowScripts |
RX.Types.WebViewSandboxMode.AllowPopups |
RX.Types.WebViewSandboxMode.AllowSameOrigin;

const styles = {
    rootStyles: RX.Styles.createViewStyle({
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'flex-start',
    }),

    webViewHeaderStyles: RX.Styles.createViewStyle({
        justifyContent: 'flex-start',
    }),

    backButtonTextStyles: RX.Styles.createTextStyle({
        color: TrackFiendStyles.color.titleTextColor,
        fontWeight: 'normal',
        fontSize: 11
    }),

    backButtonStyles: RX.Styles.createButtonStyle({
        paddingLeft: 15,
        height: 44,
        flexDirection: 'row',
        alignItems: 'center'
    }),

    scrollViewStyles: RX.Styles.createScrollViewStyle({
        height: RX.UserInterface.measureWindow().height - 44,
        width: RX.UserInterface.measureWindow().width,
    }),

    webViewStyles: RX.Styles.createWebViewStyle({
        alignItems: 'stretch',
        alignSelf: 'stretch',
        flex: 1,
        width: RX.UserInterface.measureWindow().width,
        height: RX.UserInterface.measureWindow().height - 44,
    })
}

class ViewExternalComponent extends ComponentBase<{}, ViewExternalComponentState>{
    protected _buildState(props: {}, initialBuild:false): ViewExternalComponentState{
        return ({
            url: ViewExternalStore.getUrlToRender(),
            isLoadingExternalUrl: ViewExternalStore.getIsLoadingExternalUrl()
        })
    }

    componentWillUpdate(){
        console.log("component updating");
    }

    render(): JSX.Element | null{
        return (
            <RX.View style={ styles.rootStyles }>
                <RX.View style={ [TrackFiendStyles.styles.header, 
                                RX.StatusBar.isOverlay() && 
                                TrackFiendStyles.styles.headerWithStatusBar, 
                                styles.webViewHeaderStyles] }>
                    <RX.Button onPress={ this.goBack } style={ styles.backButtonStyles }>
                        <RXImageSvg viewBox = { TrackFiendIcons.icons.chevron_left.viewBox } 
                                    height={20} width={20}>
                            <RXSvgPath d={ TrackFiendIcons.icons.chevron_left.path }
                                        fillColor= {'white'}/>
                        </RXImageSvg>
                        <RX.Text style={ styles.backButtonTextStyles }>
                            Back
                        </RX.Text>
                    </RX.Button>
                </RX.View>
                <RX.WebView url={ this.state.url }
                        sandbox={ sandboxFeatures }
                        style={ styles.webViewStyles }
                        onLoad={ this.externalContentLoaded }
                        className={ "iframe-wrapper" }/>
            </RX.View>
        )
    }

    onScrollBeginDrag = () =>{

    }

    onScrollEndDrag = () =>{
        
    }

    externalContentLoaded = (e: any)=>{
        ViewExternalStore.externalContentRendered()
    }

    renderMainContent = () =>{
        if(this.state.isLoadingExternalUrl){
            return (<RX.ActivityIndicator color={ TrackFiendStyles.color.mainAppColor }/>)
        }
        else{
            return(
                <RX.WebView url={ this.state.url }
                            sandbox={ sandboxFeatures }
                            style={ styles.webViewStyles }
                            />
            )
        }
    }

    goBack = () =>{
        ViewExternalStore.setUrlToRender("");
        NavigationStore.goBack();
    }
}

export = ViewExternalComponent