import { ComponentBase } from'resub'
import RX = require('reactxp');
import HomeComponent = require('./components/home/HomeComponent')
import ExchangeComponent = require('./components/exchange/ExchangeComponent');
import ViewExternalComponent = require('./components/view_external/ViewExternalComponent')
import NavigationRouteId = require('./enums/NavigationRouteId')
import NavigationStore from './stores/navigation/NavigationStore'
import ViewExternalStore from './stores/view_external/ViewExternalStore'
import ArtistComponent = require('./components/artist/ArtistComponent')

class App extends RX.Component<{}, null> {
    private _navigator: RX.Navigator;

    componentDidMount(){
        this._navigator.immediatelyResetRouteStack([{
            routeId: NavigationRouteId.HomePanel,
            sceneConfigType: RX.Types.NavigatorSceneConfigType.Fade
        }]);

        NavigationStore.setNavigationInstance(this._navigator);
    }

    render(): JSX.Element | null {
        return (
            <RX.Navigator
                ref= { this._onNavigatorRef }
                renderScene = { this._renderScene }
            />
        );
    }

    private _onNavigatorRef = (navigator: RX.Navigator) =>{
        this._navigator = navigator;
    }

    private _renderScene = (navigatorRoute: RX.Types.NavigatorRoute) =>{
        let sceneContext = NavigationStore.getSceneContext(navigatorRoute.routeId);
        switch(navigatorRoute.routeId){
            case(NavigationRouteId.HomePanel):
                return( <HomeComponent /> );

            case(NavigationRouteId.ViewExternal):
                return( <ViewExternalComponent /> );

            case(NavigationRouteId.Artist):
                return( <ArtistComponent {...sceneContext} /> )

            default:
                return (<HomeComponent />)
        }
    }

    getExternalUrl = ()=> {
        return ViewExternalStore.getUrlToRender();
    }
}

export = App;
