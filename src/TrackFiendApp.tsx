import RX = require('reactxp');
import App = require('./App');

import { AppRegistry } from 'react-native';

class TrackFiendApp{
    init() {
        RX.App.initialize(true, true);
        RX.UserInterface.setMainView(this._renderRootView());
    }

    private _renderRootView() {
        return (
            <App />
        );
    }
}

export = new TrackFiendApp();
