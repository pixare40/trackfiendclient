//Shared styles in the TrackFiend App

import RX = require('reactxp');

class TrackFiendStyles{
    static color = {
        mainAppColor: '#e25a57',
        manhattan: '#f2c49c',
        white: 'white',
        titleTextBackgroundColour: 'rgba(226, 90, 87, 0.72)',
        titleTextColor: 'white',
        textColor: 'black',
        defaultShadowColor: 'black',
        cardBackgroundColor: '',
        appBackgroundColour: 'white',
        defaultTabDeselectedColor: 'white',
        facebookColor: '#3b5998',
        profileHeaderBackgroundColor: '#ec5e2f',
        profileHeaderTextColor: 'white',
        listBackgroundColour: '#efefef',
        cardBorderColor: '#F5F5F5',
        twitterIconColor: '#00aced'
    };

    static styles = {
        headerWithStatusBar: RX.Styles.createViewStyle({
            paddingTop: 22
        }),

        header: RX.Styles.createViewStyle({
            backgroundColor: TrackFiendStyles.color.mainAppColor,
            flexDirection: 'row',
            height: 44,
            alignItems: 'center',
            justifyContent: 'center',
            position: 'relative'
        }),

        container: RX.Styles.createViewStyle({
            flex: 1,
            backgroundColor: TrackFiendStyles.color.white,
            alignSelf: 'stretch'
        }),

        defaultButtonStyles: RX.Styles.createButtonStyle({
            borderRadius: 5,
            borderColor: TrackFiendStyles.color.mainAppColor,
            backgroundColor: TrackFiendStyles.color.white,
        }),

        defaultButtonTextStyles: RX.Styles.createTextStyle({
            color: TrackFiendStyles.color.mainAppColor,
            fontSize: 12
        }),

        defaultTextStyles : RX.Styles.createTextStyle({
            fontSize: 11,
            color: TrackFiendStyles.color.textColor,
            fontFamily: RX.Platform.getType() == "web"? "Montserrat" : "Montserrat-Regular"
        }),

        defaultRoundedButtonStyles: RX.Styles.createButtonStyle({
            height: 40,
            justifyContent: 'flex-start',
            alignItems: 'center',
            backgroundColor: TrackFiendStyles.color.mainAppColor,
            borderRadius: 20,
            margin: 2,
            alignSelf: 'stretch'
        }),

        facebookButtonStyles:  RX.Styles.createButtonStyle({
            backgroundColor: TrackFiendStyles.color.facebookColor
        }),

        spinnerContainer: RX.Styles.createViewStyle({
            flex: 1,
            alignSelf: 'stretch',
            alignItems: 'center',
            justifyContent: 'center'
        }),

        defaultBackButtonContainerHeaderStyles: RX.Styles.createViewStyle({
            justifyContent: 'flex-start',
        }),

        defaultBackButtonStyles: RX.Styles.createButtonStyle({
            paddingLeft: 15,
            height: 44,
            flexDirection: 'row',
            alignItems: 'center'
        })
    };
}

export = TrackFiendStyles;