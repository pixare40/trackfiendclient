import NewsItem = require ('../request_clients/models/NewsItem')
import VirtualNewsListView = require('./VirtualNewsListView')

interface NewsState{
    news: Array<NewsItem>,
    virtualListNews: Array<VirtualNewsListView>
    isLoadingNews: boolean
}

export = NewsState;