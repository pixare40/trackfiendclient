import NewsItem = require ('../request_clients/models/NewsItem')
import { VirtualListViewItemInfo } from 'reactxp-virtuallistview'

interface VirtualNewsListView extends VirtualListViewItemInfo{
    index: number
    newsItem: NewsItem
}

export = VirtualNewsListView;